  /**************************************************************
 *
 * This script read AT Commands from Java and send SMS to Java
 *
 **************************************************************/


const char pinnumber[] = "";
const String END_OF_LINE = "\r\n";
const String AT_PREFIX = "AT";
const String AT_SUCCESS = "OK";
const String AT_ERROR = "ERROR";

void setup() {
  setupSerial();
}

void loop() {
  waitForATRequestsAndResponse();
}

void setupSerial() {
  // Set console baud rate
  Serial.begin(115200);

  pinMode(GSM_DTR, OUTPUT);
  digitalWrite(GSM_DTR, LOW);
  delay(5);

  // Turn on the GSM module by triggering GSM_RESETN pin
  pinMode(GSM_RESETN, OUTPUT);
  digitalWrite(GSM_RESETN, HIGH);
  delay(100);
  digitalWrite(GSM_RESETN, LOW);
  delay(2000);

  SerialGSM.begin(115200);
  delay(3000L);
  
  SerialGSM.println("ATE0"); // disable the echo
}

void waitForATRequestsAndResponse() {
  delay(1000L);
  if (SerialGSM.available()) {
    delay(1000L);
    readGSMDataAndSendToJava();
  }
  if (Serial.available()) {
    delay(1000L);
    readJavaDataAndSendToGSM();
  }
}

void readJavaDataAndSendToGSM() {
  String messageContent = "";
  int oneChar;
  while (Serial.available() > 0) {
    // Read message bytes and store them
    while ((oneChar = Serial.read()) != -1) {
      messageContent.concat((char)oneChar);    
    }
  }

  // We only send AT Commands to module
  if (messageContent.startsWith(AT_PREFIX)){
    SerialGSM.print(messageContent);
    SerialGSM.println();
  }
}

void readGSMDataAndSendToJava() {
  String messageContent = "";
  int oneChar;
  while (SerialGSM.available() > 0 && !responseEndsWithErrorOrOk(messageContent)) {
    while ((oneChar = SerialGSM.read()) != -1) {
      messageContent.concat((char)oneChar);    
    }
  }
  sendToJava(messageContent);
}

boolean responseEndsWithErrorOrOk(String response) {
  response.trim(); // Remove leading and trailing spaces
  return response.endsWith(AT_SUCCESS) || response.endsWith(AT_ERROR);
}

void sendToJava(String messageContent) {
  messageContent.trim(); // Remove leading and trailing spaces
  delay(40);
  Serial.print(messageContent + END_OF_LINE);
  Serial.println();
  delay(40);
}
