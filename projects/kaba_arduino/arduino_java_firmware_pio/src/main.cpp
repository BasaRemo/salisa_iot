/**************************************************************
 *
 * This script read AT Commands from Java and send SMS to Java
 *
**************************************************************/
#include <Arduino.h>

#include <MKRGSM.h>

const char pinnumber[] = "";
const String END_OF_LINE = "\r\n";
const String SMS_PREFIX = "SMS:";
const String AT_PREFIX = "AT";
const String AT_SUCCESS = "OK";
const String AT_ERROR = "ERROR";

const String GSM_STATUS_PREFIX = "GSM_";
const String GSM_STATUS_READY = "GSM_STATUS_READY";
const String GSM_STATUS_FAILED = "GSM_STATUS_FAILED";
const String BOOT_MSG_WAITING_STATUS = "+UMWI: 0,4";

GSM gsmAccess;
GSM_SMS sms;

boolean serialWriteLocked = false;

void setup();
void loop();
void setupSerial();
void waitForATRequestsAndResponse();
void readJavaDataAndSendToGSM();
void readGSMDataAndSendToJava();
boolean responseEndsWithErrorOrOk(String response);
void writeToSerial(String messageContent);
void checkGSMStatus();
void waitForSMS();
void connectGSM();
void reboot();

void setup() {
  setupSerial();
}

void loop() {
  waitForATRequestsAndResponse();
 checkGSMStatus();
 waitForSMS();
}

void setupSerial() {
  // Set console baud rate
  Serial.begin(115200);

  // pinMode(GSM_DTR, OUTPUT);
  // digitalWrite(GSM_DTR, LOW);
  // delay(5);

  // // Turn on the GSM module by triggering GSM_RESETN pin
  // pinMode(GSM_RESETN, OUTPUT);
  // digitalWrite(GSM_RESETN, HIGH);
  // delay(100);
  // digitalWrite(GSM_RESETN, LOW);
  // delay(2000);

  SerialGSM.begin(115200);
  delay(3000L);
  
  SerialGSM.println("ATE0"); // disable the echo
}

void waitForATRequestsAndResponse() {
  delay(1000L);
  if(!serialWriteLocked) {
    serialWriteLocked = true;
    if (SerialGSM.available()) {
      delay(1000L);
      readGSMDataAndSendToJava();
    }
    if (Serial.available()) {
      delay(1000L);
      readJavaDataAndSendToGSM();
    }
    serialWriteLocked = false;
  }
}

void readJavaDataAndSendToGSM() {
  String messageContent = "";
  int oneChar;
  while (Serial.available() > 0) {
    // Read message bytes and store them
    while ((oneChar = Serial.read()) != -1) {
      messageContent.concat((char)oneChar);    
    }
  }

  // We only send AT Commands to module
  if (messageContent.startsWith(AT_PREFIX)){
    SerialGSM.print(messageContent);
    SerialGSM.println();
  }
}

void readGSMDataAndSendToJava() {
  String messageContent = "";
  int oneChar;
  while (SerialGSM.available() > 0 && !responseEndsWithErrorOrOk(messageContent)) {
    while ((oneChar = SerialGSM.read()) != -1) {
      messageContent.concat((char)oneChar);    
    }
  }
  writeToSerial(messageContent);
}

boolean responseEndsWithErrorOrOk(String response) {
  response.trim(); // Remove leading and trailing spaces
  return response.endsWith(AT_SUCCESS) || response.endsWith(AT_ERROR);
}

void writeToSerial(String messageContent) {
  messageContent.trim(); // Remove leading and trailing spaces

  delay(40);
  if (messageContent.endsWith(AT_ERROR)){
    Serial.print("Command Failed... : " + END_OF_LINE + messageContent);
    
  } else if (messageContent.endsWith(AT_SUCCESS)){
    Serial.print("Command Success: " + END_OF_LINE + messageContent);

  } else if (messageContent.endsWith(BOOT_MSG_WAITING_STATUS)){
    Serial.print("Command Success: " + END_OF_LINE + messageContent);
    
  } else if (messageContent.startsWith(SMS_PREFIX)){
    Serial.print("Received Sms: " + END_OF_LINE + messageContent);
    
  } else if (messageContent.startsWith(GSM_STATUS_PREFIX)){
    Serial.print("GSM Status: " + END_OF_LINE + messageContent);
    
  } else {
    Serial.print(messageContent);
  }
  Serial.println();
  delay(40);
}

/** 
 *  SMS READING FUNCTIONS
 */

void waitForSMS() {
    connectGSM();
    delay(20);
    if (!serialWriteLocked && sms.available() > 0) {
      serialWriteLocked = true;
      // Get remote number
      int numSize = 20; 
      char senderNumber[numSize];
      sms.remoteNumber(senderNumber, numSize);
      senderNumber[numSize] = '\0';
      String senderNumString(senderNumber);
  
      //Combine number and message and send
      String messageContent = "";
      messageContent.concat(SMS_PREFIX);
      messageContent.concat(END_OF_LINE);
      
      messageContent.concat(senderNumString);
      messageContent.concat(END_OF_LINE);
  
      int oneChar;
      // Read message bytes and store them
      while ((oneChar = sms.read()) != -1) {
        messageContent.concat((char)oneChar);    
      }
      writeToSerial(messageContent);
      serialWriteLocked = false;
    }
    delay(1000);
}

void checkGSMStatus() {
  if (gsmAccess.status() != GSM_READY) {
    connectGSM();
  }
}

void connectGSM() {
  if(!serialWriteLocked) {
    serialWriteLocked = true;
    while (gsmAccess.begin(pinnumber) != GSM_READY) {
      // failed, retrying every 5 sec
      writeToSerial(GSM_STATUS_FAILED);
      delay(5000);
    }
    writeToSerial(GSM_STATUS_READY);
    serialWriteLocked = false;
  }
}