#include <Arduino.h>
#include <ArduinoECCX08.h>
#include <utility/ECCX08JWS.h>
#include <ArduinoMqttClient.h>
#include <Arduino_JSON.h>
#include <MKRGSM.h>

#include "arduino_secrets.h"

/////// Enter your sensitive data in arduino_secrets.h
const char pinnumber[]     = SECRET_PINNUMBER;
const char gprs_apn[]      = SECRET_GPRS_APN;
const char gprs_login[]    = SECRET_GPRS_LOGIN;
const char gprs_password[] = SECRET_GPRS_PASSWORD;

const char projectId[]     = SECRET_PROJECT_ID;
const char cloudRegion[]   = SECRET_CLOUD_REGION;
const char registryId[]    = SECRET_REGISTRY_ID;
const String deviceId      = SECRET_DEVICE_ID;

const char broker[]        = "mqtt.googleapis.com";
GSM gsmAccess;
GPRS gprs;
GSM_SMS sms;
GSMSSLClient  gsmSslClient;
MqttClient    mqttClient(gsmSslClient);

// Select your modem:
 #define TINY_GSM_MODEM_UBLOX
unsigned long lastMillis = 0;
bool pausePublish = false;
// Array to hold the number a SMS is retreived from
char senderNumber[20];

/** 
 *  METHODS
 */

void setupSerial();
void setupMttqClient();
void setupSms();
void checkGSMStatus();
void checkMttqStatus();
void connectGSM();
void connectMQTT();
void triggerMessagePublishing();
void waitForSMS();
void publishMessage();
void onMessageReceived(int messageSize);
void readAllResponseData();
void sendATRequestToNetwork();
void waitForATResponse();
void processATResponse();
void readAllResponseData();
String calculateJWT();
String calculateClientId();
unsigned long getTime();

/** 
 *  CORE FUNCTIONS
 */
 
void setup() {
  // put your setup code here, to run once:
  setupSerial();
  setupMttqClient();
  setupSms();
}

void loop() {
  // put your main code here, to run repeatedly:
  checkGSMStatus();
  checkMttqStatus();
  triggerMessagePublishing();
  waitForSMS();
}

/** 
 *  SETUP FUNCTIONS
 */

void setupSerial() {
  Serial.begin(115200);
  while (!Serial);

  if (!ECCX08.begin()) {
    Serial.println("No ECCX08 present!");
    while (1);
  }

  // Setup the Monitor serial by setting console baud rate
  Serial.begin(115200);
  // Access AT commands from Serial Monitor
  Serial.println(F("***********************************************************"));
  Serial.println(F(" You can now send AT commands"));
  Serial.println(F(" Enter \"AT\" (without quotes), and you should see \"OK\""));
  Serial.println(F(" If it doesn't work, select \"Both NL & CR\" in Serial Monitor"));
  Serial.println(F("***********************************************************"));
}

void setupMttqClient(){
  // Calculate and set the client id used for MQTT
  String clientId = calculateClientId();

  mqttClient.setId(clientId);

  // Set the message callback, this function is
  // called when the MQTTClient receives a message
  mqttClient.onMessage(onMessageReceived);
}

void setupSms() {
  connectGSM();
  Serial.println("Waiting for SMS");
}


/** 
 *  GSM & GPRS STATUS AND CONNECTION FUNCTIONS
 */
 
void checkGSMStatus() {
  if (gsmAccess.status() != GSM_READY || gprs.status() != GPRS_READY) {
    connectGSM();
  }
}

void checkMttqStatus() {
  if (!mqttClient.connected()) {
    // MQTT client is disconnected, connect
    connectMQTT();
  }
  // poll for new MQTT messages and send keep alives
  mqttClient.poll();
}

void triggerMessagePublishing() {
  // publish a message roughly every 5 seconds.
  if ((millis() - lastMillis > 10000) && !pausePublish) {
    lastMillis = millis();
    delay(20);
    publishMessage();
  }
}

/** 
 *  SMS READING FUNCTIONS
 */
 
void waitForSMS() {
    delay(20);
    int c;

  // If there are any SMSs available()
  if (sms.available()) {
    Serial.println("Message received from:");

    // Get remote number
    sms.remoteNumber(senderNumber, 20);
    Serial.println(senderNumber);

    // An example of message disposal
    // Any messages starting with # should be discarded
    if (sms.peek() == '#') {
      Serial.println("Discarded SMS");
//      sms.flush();
    }

    // Read message bytes and print them
    while ((c = sms.read()) != -1) {
      Serial.print((char)c);
    }

    Serial.println("\nEND OF MESSAGE");

    // Delete message from modem memory
//    sms.flush();
//    Serial.println("MESSAGE DELETED");
  }

  delay(1000);
}

void connectGSM() {
  Serial.println("Attempting to connect to the cellular network");

  while ((gsmAccess.begin(pinnumber) != GSM_READY) ||
         (gprs.attachGPRS(gprs_apn, gprs_login, gprs_password) != GPRS_READY)) {
    // failed, retry
    Serial.print("Not connected to GSM retrying every 10 second");
    delay(10000);
  }

  Serial.println("You're connected to the cellular network");
  Serial.println();
}

/** 
 *  MTTQ STATUS AND CONNECTION FUNCTIONS
 */
 
void connectMQTT() {
  Serial.print("Attempting to connect to MQTT broker: ");
  Serial.print(broker);
  Serial.println(" ");
  int attempts = 0;

  while (!mqttClient.connected()) {
    // Calculate the JWT and assign it as the password
    String jwt = calculateJWT();

    mqttClient.setUsernamePassword("", jwt);

    if (!mqttClient.connect(broker, 8883) && attempts < 5) {
      // failed, retry
      Serial.print(".");
      attempts++;
      delay(5000);
    } else {
      connectGSM();
      attempts = 0;
    }
  }
  Serial.println();

  Serial.println("You're connected to the MQTT broker");
  Serial.println();

  // subscribe to topics
  int subscribeQos = 1;
  mqttClient.subscribe("/devices/" + deviceId + "/config", 1);
  mqttClient.subscribe("/devices/" + deviceId + "/commands/#");
//  mqttClient.subscribe("/vodacom");
}

/** 
 *  MTTQ PUBLISH AND SUBSCRIBE CALLBACK FUNCTIONS
 */
 
void publishMessage() {
  Serial.println("Publishing message");

  // send message, the Print interface can be used to set the message contents
  mqttClient.beginMessage("/devices/" + deviceId + "/events");
//  mqttClient.beginMessage("/devices/" + deviceId + "/state");
  mqttClient.print("Transaction succeded for 100$ ");
  mqttClient.print(millis());
  mqttClient.endMessage();
}

void onMessageReceived(int messageSize) {
  pausePublish = true;
  delay(20);
  while(millis() - lastMillis < 40) {
    Serial.println("Message received: ");
    Serial.println("Waiting 40ms from last sent message before reading the message...");
  }
  String messageTopic = mqttClient.messageTopic();
  Serial.println("Reading the message...");
  // we received a message, print out the topic and contents
  Serial.print("Received a message with topic '");
  Serial.print(mqttClient.messageTopic());
  Serial.print("', length ");
  Serial.print(messageSize);
  Serial.println(" bytes:");

  // use the Stream interface to print the contents
  while (mqttClient.available()) {
    Serial.print((char)mqttClient.read());
  }
  Serial.println();
  Serial.println();
  lastMillis = millis();
//    if(messageTopic == "/devices/BasaMKRGSM1400/commands") {
  if(messageTopic == "/devices/arduino_ntambwa/commands") {
    sendATRequestToNetwork();
    waitForATResponse();
    publishMessage();
  }
  pausePublish = false;
}

void sendATRequestToNetwork() {
  SerialGSM.println("AT");
  delay(2000);
  
  SerialGSM.println("AT+CMGF=1"); // Configuring TEXT mode
  delay(2000);
  
  SerialGSM.println("AT+CSCS=\"GSM\"");
  delay(2000);
  
  SerialGSM.println("AT+CUSD=1");
  delay(2000);
  
  SerialGSM.println("AT+CSQ");
  delay(2000);
  SerialGSM.println("AT+CUSD=1,\"*101*3*4*1#\",15");
  delay(10000);
}

void waitForATResponse() {
  while (!SerialGSM.available()) {}
  processATResponse();
}

void processATResponse() {
    if (SerialGSM.available()) {
      delay(20);
      readAllResponseData();
    }
    delay(20);
}

void readAllResponseData() {
  int commandSize = 2000;
  char messageBytes[commandSize];
  int index = 0;
  while (SerialGSM.available()) {
    messageBytes[index] = SerialGSM.read();
    index++;
  }
  Serial.println("AT & USSD RESPONSE ARRIVED!!");
  Serial.write(messageBytes, index);
  Serial.println();
}

String calculateClientId() {
  String clientId;

  // Format:
  //
  //   projects/{project-id}/locations/{cloud-region}/registries/{registry-id}/devices/{device-id}
  //

  clientId += "projects/";
  clientId += projectId;
  clientId += "/locations/";
  clientId += cloudRegion;
  clientId += "/registries/";
  clientId += registryId;
  clientId += "/devices/";
  clientId += deviceId;

  return clientId;
}

String calculateJWT() {
  unsigned long now = getTime();
  
  // calculate the JWT, based on:
  //   https://cloud.google.com/iot/docs/how-tos/credentials/jwts
  JSONVar jwtHeader;
  JSONVar jwtClaim;

  jwtHeader["alg"] = "ES256";
  jwtHeader["typ"] = "JWT";

  jwtClaim["aud"] = projectId;
  jwtClaim["iat"] = now;
  jwtClaim["exp"] = now + (24L * 60L * 60L); // expires in 24 hours 

  return ECCX08JWS.sign(0, JSON.stringify(jwtHeader), JSON.stringify(jwtClaim));
}

unsigned long getTime() {
  // get the current time from the cellular module
  return gsmAccess.getTime();
}