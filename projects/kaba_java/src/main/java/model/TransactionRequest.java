package model;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.Builder;
import lombok.ToString;

@Getter @Setter @ToString  //@Builder @AllArgsConstructor(access = AccessLevel.PUBLIC)
public class TransactionRequest {
  private String transactionId;
  private String firstName;
  private String lastName;
  private String country;
  private String status;
  private String provider;
  private long amount;
  private long number;
  private int retry = 0;
}
