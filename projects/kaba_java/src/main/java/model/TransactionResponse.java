package model;
import java.util.Optional;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.Builder;

@Getter @Setter @Builder @AllArgsConstructor(access = AccessLevel.PUBLIC)
public class TransactionResponse {
  private String transactionId;
  private String status;
  private String notes;
  private int retry;
}
