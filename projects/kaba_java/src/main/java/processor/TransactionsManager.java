package processor;

import factory.ModemFactory;
import model.TransactionRequest;

public class TransactionsManager {
  private final TransactionsProcessor transactionsProcessor;

  public TransactionsManager() {
    ModemFactory  modemFactory = new ModemFactory();
    this.transactionsProcessor = new TransactionsProcessor(modemFactory.getModemDevices());
  }

  public synchronized void addTransactionRequest(TransactionRequest transactionRequest) {
    // TODO: Ntambwa: Add a processor for each provider and assign the request to the right one
    transactionsProcessor.addTransactionRequest(transactionRequest);
  }
}
