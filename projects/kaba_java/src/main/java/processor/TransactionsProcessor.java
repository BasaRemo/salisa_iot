package processor;

import device.ModemResponseType;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import loggers.ProcessorLogger;
import model.TransactionResponse;
import pubsub.PublisherService;
import device.ModemDevice;
import java.util.LinkedList;
import java.util.Queue;
import model.TransactionRequest;

enum TransactionStatus {
  BEGIN,                      // Transaction is paid for but not processed on firestore/pub/sub yet
  PROCESSING,                 // When transaction send to firestore
  PROCESSING_ERROR,           // When we fail to deliver message from pubsub to java
  NETWORK_SENT,               // when we receive ussd OK response
  NETWORK_PROCESSING_ERROR,   // when ussd returns ERROR
  NETWORK_COMPLETED,          // When we received the sms confirmation
}

class TransactionsProcessor {
  static final ExecutorService REQUEST_EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
  static final ExecutorService RESPONSE_EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
  private static final ProcessorLogger PROCESSOR_LOGGER = new ProcessorLogger();
  private final List<TransactionRequest> transactionsRequests;
  private final List<ModemDevice> modemDevices;

  TransactionsProcessor(List<ModemDevice> modemDevices) {
    this.modemDevices = modemDevices;
    this.transactionsRequests = Collections.synchronizedList(new ArrayList<>());
    startProcessor();
  }

  private void startProcessor() {
    for (ModemDevice modemDevice : modemDevices) {
      modemDevice.listenToModemState().subscribe(modemState -> {
        PROCESSOR_LOGGER.logModemStateUpdate(modemState.toString(), modemDevice.getSerialPortName());
        triggerTransactionProcess(modemDevice);
      });
    }
  }

  private synchronized void findModemToProcessTransaction() {
    for (ModemDevice modemDevice : modemDevices) {
      if (modemDevice.isModemReady()) {
        triggerTransactionProcess(modemDevice);
        break;
      }
    }
  }

  private synchronized void triggerTransactionProcess(ModemDevice modemDevice) {
    if (modemDevice.isModemReady() && !transactionsRequests.isEmpty()) {
      TransactionRequest transactionRequest = transactionsRequests.remove(0);
      PROCESSOR_LOGGER.logTransactionsRequestsRemaining(transactionsRequests.size());
      processTransaction(transactionRequest, modemDevice);
    }
  }

  private  void processTransaction(TransactionRequest transactionRequest, ModemDevice modemDevice) {
    modemDevice.sendMobileMoneyTransfer(transactionRequest)
        .observeOn(Schedulers.from(TransactionsProcessor.RESPONSE_EXECUTOR_SERVICE))
        .subscribe(transactionWrapper ->
            handleTransactionResponse(transactionWrapper.getRequest(),
                transactionWrapper.getResponse()));
  }

  private synchronized void handleTransactionResponse(TransactionRequest request, TransactionResponse response) {
    String transactionId = response.getTransactionId();
    boolean isFailed = response.getNotes().contains(ModemResponseType.ERROR.toString());
    int retry = request.getRetry();
    if(isFailed && transactionId.equals(request.getTransactionId())
        &&  request.getRetry() < 3) {
      retry += 1;
      request.setRetry(retry);
      PROCESSOR_LOGGER.logTransactionRequestRetry(retry);
      transactionsRequests.add(request);
    } else if (isFailed) {
      response.setStatus(TransactionStatus.NETWORK_PROCESSING_ERROR.toString());
      response.setRetry(retry);
      response.setTransactionId(request.getTransactionId());
      PublisherService.publishTransactionResponse(response);
    } else {
      response.setStatus(TransactionStatus.NETWORK_SENT.toString());
      response.setRetry(retry);
      response.setTransactionId(request.getTransactionId());
      PublisherService.publishTransactionResponse(response);
    }
    PROCESSOR_LOGGER.logTransactionsRequestsRemainingAfterHandle(transactionsRequests.size());
  }

  synchronized void addTransactionRequest(TransactionRequest transactionRequest) {
    Runnable runnableTask = () -> {
      try {
        transactionsRequests.add(transactionRequest);
        PROCESSOR_LOGGER.logTransactionsRequestsCount(transactionsRequests.size());
        findModemToProcessTransaction();
      } catch (Exception e) {
        PROCESSOR_LOGGER.logException(e);
      }
    };
    TransactionsProcessor.REQUEST_EXECUTOR_SERVICE.execute(runnableTask);
  }
}
