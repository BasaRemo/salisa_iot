package home;

import server.MobileMoneyServer;

public class Main {
    public static void main(String[] args) {
        MobileMoneyServer mobileMoneyServer = new MobileMoneyServer();
        mobileMoneyServer.start();
    }
}
