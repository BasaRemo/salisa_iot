package factory;

import com.fazecast.jSerialComm.SerialPort;
import device.ModemDevice;
import loggers.ModemLogger;
import java.util.ArrayList;
import java.util.List;

public class ModemFactory {
  private final static String PORT_NAME_PREFIX = "tty.usbmodem14";
  private final static ModemLogger MODEM_LOGGER = new ModemLogger();
  private List<ModemDevice> modemDevices;

  public ModemFactory() {
    modemDevices = createModemDevices();
  }

  private synchronized List<ModemDevice> createModemDevices() {
    List<ModemDevice> modems = new ArrayList<>();
    for (SerialPort port : SerialPort.getCommPorts()) {
      if (port.getSystemPortName().contains(PORT_NAME_PREFIX)) {
        port.setComPortParameters(115200, 8, 1, SerialPort.NO_PARITY);
        modems.add(new ModemDevice(port));
      }
    }
    if (modems.isEmpty()) {
      MODEM_LOGGER.logFailedConnectToSerial();
    } else {
      MODEM_LOGGER.logSuccessConnectToSerial(modems.size());
    }
    return modems;
  }

  public List<ModemDevice> getModemDevices() {
    return modemDevices;
  }
}
