package pubsub;

public enum SubscriptionChannel {
  COD_243("cod243_sub"),
  AIRTEL_CD("airtelsub"),
  ORANGE_CD("orangesub");

  private final String subscriptionChannel;

  SubscriptionChannel(final String subscriptionChannel) {
    this.subscriptionChannel = subscriptionChannel;
  }

  @Override
  public String toString() {
    return subscriptionChannel;
  }
}
