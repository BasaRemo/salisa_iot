package pubsub;

import com.google.cloud.ServiceOptions;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.pubsub.v1.ProjectSubscriptionName;
import loggers.PubSubLogger;
import processor.TransactionsManager;

public final class SubscriptionService {
  private static final String PROJECT_ID = ServiceOptions.getDefaultProjectId();
  private static final PubSubLogger PUB_SUB_LOGGER = new PubSubLogger();
  private final TransactionsManager transactionsManager;

  public SubscriptionService(TransactionsManager transactionsManager) {
    this.transactionsManager = transactionsManager;
  }

  public void subscribeToTopic(SubscriptionChannel channel) {
    subscribeToPubSubTopicSubscription(channel);
  }

  /** Receive messages over a subscription. */
  private void subscribeToPubSubTopicSubscription(SubscriptionChannel channel) {
    String subscriptionId = channel.toString();
    ProjectSubscriptionName subscriptionName =
        ProjectSubscriptionName.of(PROJECT_ID, subscriptionId);

    PUB_SUB_LOGGER.logSubscribedToTopicSubscription(subscriptionId);
    // create a subscriber bound to the asynchronous message receiver
    Subscriber subscriber = Subscriber
        .newBuilder(subscriptionName,
            MessagePubSubReceiver
                .builder()
                .transactionsManager(transactionsManager)
                .build())
        .build();
    subscriber.startAsync().awaitRunning();
    // Allow the subscriber to run indefinitely unless an unrecoverable error occurs.
    subscriber.awaitTerminated();
  }
}
