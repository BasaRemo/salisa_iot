package pubsub;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutures;
import com.google.cloud.ServiceOptions;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.gson.Gson;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import java.util.ArrayList;
import java.util.List;
import loggers.PubSubLogger;
import model.TransactionResponse;

public class PublisherService {
  private static final PubSubLogger PUB_SUB_LOGGER = new PubSubLogger();
  private static final String PROJECT_ID = ServiceOptions.getDefaultProjectId();

  public static synchronized void publishTransactionResponse(TransactionResponse transactionResponse) {
    try {
      publishMessage(transactionResponse);
    } catch (Exception e) {
      PUB_SUB_LOGGER.logPublisherPublishException(e);
    }
  }
   //Publish messages to a topic.
  private static synchronized void publishMessage(TransactionResponse transactionResponse) throws Exception {
    String topicId = "transactions_updates";
    ProjectTopicName topicName = ProjectTopicName.of(PROJECT_ID, topicId);
    Publisher publisher = null;
    List<ApiFuture<String>> futures = new ArrayList<>();

    try {
      // Create a publisher instance with default settings bound to the topic
      publisher = Publisher.newBuilder(topicName).build();

      Gson gson = new Gson();
      String responseJson = gson.toJson(transactionResponse);
      PUB_SUB_LOGGER.logResponseJson(responseJson);

      // convert message to bytes
      ByteString data = ByteString.copyFromUtf8(responseJson);
      PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

      // Schedule a message to be published. Messages are automatically batched.
      ApiFuture<String> future = publisher.publish(pubsubMessage);
      futures.add(future);
    } finally {
      // Wait on any pending requests
      List<String> messageIds = ApiFutures.allAsList(futures).get();
      PUB_SUB_LOGGER.logPublishPendingRequestsIds(messageIds);

      if (publisher != null) {
        // When finished with the publisher, shutdown to free up resources.
        publisher.shutdown();
      }
    }
  }
}
