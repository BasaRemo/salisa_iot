package pubsub;

import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.gson.Gson;
import com.google.pubsub.v1.PubsubMessage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import loggers.PubSubLogger;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import model.TransactionRequest;
import org.apache.commons.lang3.StringEscapeUtils;
import processor.TransactionsManager;

@Builder @AllArgsConstructor(access = AccessLevel.PUBLIC)
public class MessagePubSubReceiver implements MessageReceiver {
  static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(2);
  private static final PubSubLogger PUB_SUB_LOGGER = new PubSubLogger();
  private TransactionsManager transactionsManager;

  @Override
  public void receiveMessage(PubsubMessage messageData, AckReplyConsumer consumer) {
    Runnable runnableTask = () -> {
      try {
        String request = messageData.getData().toStringUtf8();
        PUB_SUB_LOGGER.logMessageReceivedOnThread(request, messageData.getMessageId());
        TransactionRequest transactionRequest = buildTransactionRequest(request);
        transactionsManager.addTransactionRequest(transactionRequest);
        consumer.ack();
      } catch (Exception e) {
        PUB_SUB_LOGGER.logReceivedDataError(e);
      }
    };
    MessagePubSubReceiver.EXECUTOR_SERVICE.execute(runnableTask);
  }

  private TransactionRequest buildTransactionRequest(String transactionRequestJson) {
    Gson gson = new Gson();
    TransactionRequest request =  gson.fromJson(StringEscapeUtils.unescapeJson(transactionRequestJson), TransactionRequest.class);
    PUB_SUB_LOGGER.logTransactionRequestBuilder(request);
    return request;
  }
}
