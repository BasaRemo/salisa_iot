package device;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import io.reactivex.Observable;
import io.reactivex.Emitter;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import loggers.ModemLogger;
import model.TransactionRequest;
import model.TransactionResponse;
import model.TransactionWrapper;
import org.javatuples.Pair;

public class ModemDevice {
  static final ExecutorService READ_EXECUTOR_SERVICE = Executors.newFixedThreadPool(2);
  private final static ModemLogger MODEM_LOGGER = new ModemLogger();
  private ModemState modemState;
  private SerialPort serialPort;
  private Disposable modemSetupDisposable;
  private PublishSubject<Optional<String>> modemSetupSubject;
  private PublishSubject<TransactionWrapper> transactionResponseSubject;
  private PublishSubject<ModemState> modemStateSubject;
  private Optional<TransactionRequest> currentRequest;

  public ModemDevice(SerialPort serialPort) {
    modemState = ModemState.UNAVAILABLE;
    modemSetupSubject = PublishSubject.create();
    modemStateSubject = PublishSubject.create();
    transactionResponseSubject = PublishSubject.create();
    this.serialPort = serialPort;
    currentRequest = Optional.empty();
    setupSerial();
  }

  private void setupSerial() {
    MODEM_LOGGER.logInitDeviceCommunication();
    updateState(ModemState.INITIALIZING);
    openArduinoPort();
    listenToDataReceivedEvent();
    MODEM_LOGGER.logDeviceCommunicationCompleted();
    setupUSSD();
  }

  private void openArduinoPort() {
    try {
      MODEM_LOGGER.logComPortInfo(serialPort.getSystemPortName(), serialPort.getDescriptivePortName());
      if (serialPort.openPort()) {
        MODEM_LOGGER.logPortIsOpen();
      } else {
        MODEM_LOGGER.logFailedToOpenPort();
        return;
      }
      Thread.sleep(2000);

    } catch (Exception e) {
      System.err.println(e.toString());
    }
  }

  private synchronized void setupUSSD() {
    MODEM_LOGGER.logSetupStart();
    Queue<String> setupCommands = getModemSetupCommands();
    String command = setupCommands.poll();
    sendUSSDCommand(command);

    modemSetupDisposable = modemSetupSubject
        .subscribe(result -> {
          MODEM_LOGGER.logSerialResponse(result.get(), getSerialPortName());
          if(!setupCommands.isEmpty()) {
            String nextCommand = setupCommands.poll();
            sendUSSDCommand(nextCommand);
          } else {
            MODEM_LOGGER.logSetupEnd();
            modemSetupSubject.onComplete();
            modemSetupDisposable.dispose();
            updateState(ModemState.FREE);
          }
        });
  }

  private Queue<String> getModemSetupCommands() {
    String testAtCommand = "AT";
    String setGsmModemToPDUMode = "AT+CMGF=0"; // O: PDU Mode,  1: Text Mode
    String setCharacterSetToGSM = "AT+CSCS=\"GSM\"";
    String getSignalStrength = "AT+CSQ";

    Queue<String> setupCommands = new LinkedList<>();
    setupCommands.add(testAtCommand);
    setupCommands.add(setGsmModemToPDUMode);
    setupCommands.add(setCharacterSetToGSM);
    setupCommands.add(getSignalStrength);
    return setupCommands;
  }

  public synchronized void listenToDataReceivedEvent() {
    Runnable runnableTask = () -> {
      try {
        serialPort.addDataListener(new SerialPortDataListener() {
          @Override
          public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_RECEIVED; }
          @Override
          public void serialEvent(SerialPortEvent event) {
            handleSerialResponse(event);
          }
        });
      } catch (Exception e) {
        MODEM_LOGGER.logCreateModemException(e);
      }
    };
    ModemDevice.READ_EXECUTOR_SERVICE.execute(runnableTask);
  }

  private void handleSerialResponse(SerialPortEvent event) {
    byte[] newData = event.getReceivedData();
    String message = new String(newData);
    if (message.startsWith(ModemResponseType.SUCCESS.toString()) || message.startsWith(ModemResponseType.ERROR.toString())){
      if (!modemSetupDisposable.isDisposed()) {
        modemSetupSubject.onNext(Optional.of(message));
      } else if(currentRequest.isPresent()) {
        TransactionResponse response = TransactionResponse
            .builder().transactionId(currentRequest.get().getTransactionId()).notes(message).build();
        TransactionWrapper transactionWrapper = TransactionWrapper.builder()
            .request(currentRequest.get())
            .response(response)
            .build();
        transactionResponseSubject.onNext(transactionWrapper);
        updateState(ModemState.FREE);
        MODEM_LOGGER.logSerialResponse(message, getSerialPortName());
      }
    } else if (message.startsWith(ModemResponseType.SMS_PREFIX.toString())) {
      MODEM_LOGGER.logReceivedSMS(message);
    } else if (message.startsWith(ModemResponseType.GSM_STATUS.toString())) {
      MODEM_LOGGER.logReceivedGSMStatus(message);
    } else {
      MODEM_LOGGER.logUnrecognizeMessage(message);
    }
  }

  private void sendUSSDCommand(String ussdCommand) {
    try {
      if (serialPort.isOpen()) {
        updateState(ModemState.BUSY);
        Thread.sleep(100);
        serialPort.getOutputStream().write(ussdCommand.getBytes());
        Thread.sleep(10);
        serialPort.getOutputStream().flush();
        Thread.sleep(100);
        MODEM_LOGGER.logUSSDCommandSent(ussdCommand, getSerialPortName());
      } else {
        updateState(ModemState.FREE);
        MODEM_LOGGER.logFailedUSSDCommand();
      }
    } catch (Exception e) {
      MODEM_LOGGER.logSendUSSDException(e);
    }
  }

  private synchronized void updateState(ModemState newState) {
    modemState = newState;
    modemStateSubject.onNext(newState);
  }

  public String getSerialPortName() {
    return serialPort.getSystemPortName();
  }

  public synchronized PublishSubject<ModemState> listenToModemState() {
    return modemStateSubject;
  }

  public synchronized boolean isModemReady() {
    return modemState == ModemState.FREE;
  }

  public synchronized Observable<TransactionWrapper> sendMobileMoneyTransfer(TransactionRequest transactionRequest) {
    currentRequest = Optional.of(transactionRequest);
    // TODO: Ntambwa Update to support multiple providers and mobile money ussd commands
    String orangeMenuCommand = "AT+CUSD=1,\"*101*3#\",15";
    sendUSSDCommand(orangeMenuCommand);
    return transactionResponseSubject;
  }
}
