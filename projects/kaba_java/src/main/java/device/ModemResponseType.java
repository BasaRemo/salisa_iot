package device;

public enum ModemResponseType {
  SUCCESS("Command Success"),
  ERROR("Command Failed"),
  GSM_STATUS("GSM Status"),
  SMS_PREFIX("Received Sms");

  private final String response;

  ModemResponseType(final String response) {
    this.response = response;
  }

  @Override
  public String toString() {
    return response;
  }
}
