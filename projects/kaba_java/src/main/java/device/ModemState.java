package device;

public enum ModemState {
  UNAVAILABLE,
  INITIALIZING,
  FREE,
  BUSY
}
