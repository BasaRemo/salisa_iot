package server;
import pubsub.SubscriptionChannel;
import pubsub.SubscriptionService;
import processor.TransactionsManager;

public final class MobileMoneyServer {
  private final SubscriptionService subscriptionService;

  public MobileMoneyServer() {
    subscriptionService = new SubscriptionService(new TransactionsManager());
  }

  public void start() {
    subscriptionService.subscribeToTopic(SubscriptionChannel.COD_243);
  }
}
