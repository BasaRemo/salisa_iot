package loggers;

public class ProcessorLogger extends ParentLogger {
  public ProcessorLogger() {
    isDisabled = true;
  }

  public  void logProcessingTransaction() {
    if (isDisabled) { return; }
    System.out.println("TransactionsProcessor processing transactions" + addThreadInfo());
  }

  public void logAddTransactionRequestManager() {
    if (isDisabled) { return; }
    System.out.println("TransactionsManager.addTransactionRequest" + addThreadInfo());
  }

  public void logAddTransactionRequestProcessor() {
    if (isDisabled) { return; }
    System.out.println("TransactionsProcessor.addTransactionRequest" + addThreadInfo());
  }

  public void logListenToModemState() {
    if (isDisabled) { return; }
    System.out.println("TransactionsProcessor modemDevice.listenToModemState" + addThreadInfo());
  }

  public void logModemStateUpdate(String state, String serialName) {
    if (isDisabled) { return; }
    System.out.println("ModemDevice: " + serialName + "  State Update: " + state + addThreadInfo());
  }

  public void logTransactionRequestRetry(int retryCount) {
//    if (isDisabled) { return; }
    System.out.println("Transaction request failed: " + retryCount + "  time. Retrying.. " + addThreadInfo());
  }

  public void logTransactionsRequestsRemaining(int remaining) {
//    if (isDisabled) { return; }
    System.out.println("transactionsRequests: " + remaining + " remaining after removing one" + addThreadInfo());
  }

  public void logTransactionsRequestsRemainingAfterHandle(int remaining) {
//    if (isDisabled) { return; }
    System.out.println("transactionsRequests: " + remaining + " remaining after handling response" + addThreadInfo());
  }

  public void logTransactionsRequestsCount(int count) {
//    if (isDisabled) { return; }
    System.out.println("addTransactionRequest: " + count + "  tasks after adding " + addThreadInfo());
  }

  public void logException(Exception e) {
    if (isDisabled) { return; }
    System.out.println("Transactions process exception " + e);
  }
}
