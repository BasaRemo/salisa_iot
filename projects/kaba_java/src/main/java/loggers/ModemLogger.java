package loggers;

public final class ModemLogger extends ParentLogger {
  public ModemLogger() {
    isDisabled = false ;
  }

  public void logInitDeviceCommunication() {
    if (isDisabled) { return; }
    System.out.println("************************************");
    System.out.println("initializing device communication..." + addThreadInfo());
  }

  public void logDeviceCommunicationCompleted() {
    if (isDisabled) { return; }
    System.out.println("device initialization Completed.." + " in thread: " + addThreadInfo());
  }

  public void logFailedConnectToSerial() {
    if (isDisabled) { return; }
    System.out.println("Failed to connect to Serial Ports...");
  }

  public void logSuccessConnectToSerial(int numberPorts) {
    if (isDisabled) { return; }
    System.out.println("Connected to " + numberPorts + " ports");
  }

  public void logSetupStart() {
    if (isDisabled) { return; }
    System.out.println("************************************");
    System.out.println("Starting USSD Setup...");
  }

  public void logSetupEnd() {
    if (isDisabled) { return; }
    System.out.println("Completed USSD Setup..." + ", in thread: " + addThreadInfo());
    System.out.println("************************************");
    System.out.println();
  }

  public void logUSSDCommandSent(String ussdCommand, String serialPortName) {
    if (isDisabled) { return; }
    System.out.println("Mobile Money Command: " + ussdCommand + " sent." + " in thread: " + addThreadInfo() + addSerialInfo(serialPortName));
  }

  public void logFailedUSSDCommand() {
    if (isDisabled) { return; }
    System.out.println("Failed to send USSD command.. Port is not open..");
  }

  public void logComPortInfo(String systemPortName, String descriptivePortName) {
    if (isDisabled) { return; }
    System.out.println("COM Ports: " + systemPortName + " in thread: " + addThreadInfo());
    System.out.println("COM Ports: " + descriptivePortName);
  }

  public void logPortIsOpen() {
    if (isDisabled) { return; }
    System.out.println("Port is open..");
  }

  public void logFailedToOpenPort() {
    if (isDisabled) { return; }
    System.out.println("Failed to open port...");
  }

  public void logReceivedSMS(String sms) {
    if (isDisabled) { return; }
    System.out.println("Received SMS: " + sms);
  }

  public void logUnrecognizeMessage(String message) {
    if (isDisabled) { return; }
    System.out.println("Received Unrecognized message from Serial: " + message);
  }

  public void logReceivedGSMStatus(String message) {
    if (isDisabled) { return; }
    System.out.println("Receive GSM Status: " + message);
  }

  public void logSerialResponse(String message, String serialPortName) {
    if (isDisabled) { return; }
    System.out.println("Received Response: " + message + addThreadInfo() + addSerialInfo(serialPortName));
  }

  public void logSendUSSDException(Exception e) {
    if (isDisabled) { return; }
    System.out.println("ModemDevice.sendUSSDCommand Exception: " + e.toString());
  }

  public void logCreateModemException(Exception e) {
    if (isDisabled) { return; }
    System.out.println("ModemFactory.addModem exception : " + e);
  }

  public String addSerialInfo(String SerialPortName) {
    return " with SerialPortName: " + SerialPortName;
  }
}
