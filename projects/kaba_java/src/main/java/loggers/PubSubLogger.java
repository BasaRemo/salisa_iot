package loggers;

import java.util.List;
import model.TransactionRequest;

public class PubSubLogger extends ParentLogger {
  public PubSubLogger() {
    isDisabled = true;
  }

  public void logSubscribedToTopicSubscription(String subscription) {
    if (isDisabled) { return; }
    System.out.println("SubscriptionService subscribeToTopic: " + subscription + ", in thread: " + Thread.currentThread().getName());
  }

  public void logMessageReceivedOnThread(String message, String messageId) {
    if (isDisabled) { return; }
    System.out.println("MessagePubSubReceiver.receiveMessage Thread: " + Thread.currentThread().getName());
    System.out.println(
        "Received transaction with ID: " + messageId + "\nand data: \n" + message);
  }

  public void logSubscriptionError(IllegalStateException e) {
    if (isDisabled) { return; }
    System.out.println("Subscriber unexpectedly stopped: " + e);
  }

  public void logPublisherPublishException(Exception e) {
    if (isDisabled) { return; }
    System.out.println("Publisher unexpectedly stopped: " + e);
  }

  public void logReceivedDataError(Exception e) {
    if (isDisabled) { return; }
    System.out.println("MessagePubSubReceiver.receiveMessage exception : " + e);
  }

  public void logPublishPendingRequestsIds(List<String> pendingPublishRequestsIds) {
    if (isDisabled) { return; }
    System.out.println("PublisherService pending publish requests ids : ");
    for (String publishId : pendingPublishRequestsIds) {
      System.out.print(", " + publishId);
    }
  }

  public void logResponseJson(String response) {
    if (isDisabled) { return; }
    System.out.println("PublisherService.publishMessage: " + response);
  }

  public void logTransactionRequestBuilder(TransactionRequest request) {
    if (isDisabled) { return; }
    System.out.println("MessagePubSubReceiver buildTransactionRequest: \n" + request);
  }

}
