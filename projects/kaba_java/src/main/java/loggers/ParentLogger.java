package loggers;

public class ParentLogger {
  boolean isDisabled = false;

  String addThreadInfo() {
    return " in thread: " + Thread.currentThread().getName();
  }
}
