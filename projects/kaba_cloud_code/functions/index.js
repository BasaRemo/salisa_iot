const {PubSub} = require('@google-cloud/pubsub');
const functions = require('firebase-functions');
// Creates a client; cache this for further use
const pubSub = new PubSub();

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

const TRANSACTION_TOPIC = "transactions";
const TRANSACTION_UPDATE_TOPIC = "transactions_updates";
const COD_243_TOPIC = "cod243";

const transactionStatus = {
  BEGIN: 'BEGIN',
  PROCESSING: 'PROCESSING',
  PROCESSING_ERROR: 'PROCESSING_ERROR',
  NETWORK_SENT: 'NETWORK_SENT',
  NETWORK_PROCESSING_ERROR: 'NETWORK_PROCESSING_ERROR',
  NETWORK_COMPLETED: 'NETWORK_COMPLETED'
}

/***********************************
* Cloud Firestore
************************************/

exports.onTransactionCreatedDoc = functions.firestore.document('/transactions/{transactionId}')
    .onCreate((snapshot, context) => {
      const transaction = snapshot.data();
      const transactionId = context.params.transactionId;
      console.log('transaction: ', JSON.stringify(transaction));
      console.log('transactionId: ', JSON.stringify(transactionId));

      var transactionToSend = {
        transactionId : transactionId,
        country: transaction.countrySelected,
        firstName: transaction.firstName,
        lastName: transaction.lastName,
        provider: transaction.providerSelected,
        status : transactionStatus.PROCESSING,
        number: parseInt(transaction.phoneNbr),
        amount: parseInt(transaction.amount),
        retry: 0
      };
      
      publishMessage(JSON.stringify(transactionToSend));
      return snapshot.ref.set({ status: transactionStatus.PROCESSING }, {merge: true});
});

/* Publish message to PubSub topic */
async function publishMessage(data) {
  const topicName = COD_243_TOPIC;

  // Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
  const dataBuffer = Buffer.from(data);

  const transactionId = await pubSub.topic(topicName).publish(dataBuffer);
  console.log(`Transaction ${transactionId} published to topic ${topicName}.`);
}

/***********************************
* PubSub
************************************/

/* Subscribe to confirmations topic from the device */
exports.onPublishTransactionUpdate = functions.pubsub.topic(TRANSACTION_UPDATE_TOPIC).onPublish((message) => {
  console.log('message.data base64: ' + JSON.stringify(message.data));
  
  const messageBody = message.data ? Buffer.from(message.data, "base64").toString() : null;
  console.log('message.body: ' + messageBody);
  let json;
  try {
      json = JSON.parse(messageBody);
  } catch (e) {
      // Handle JSON error.
      console.log('json parsing error: ' + e);
  }
  console.log('json: ' + JSON.stringify(json));
  updateTransactionWithStatus(json);
  return null;
});

/* Update the transaction on firestore with a new status */
function updateTransactionWithStatus(data) {
  const transactionUpdateItem = {
    transactionId : data.transactionId,
    status : data.status,
    notes: data.notes,
    retry: data.retry
  };
  console.log("updateTransactionWithStatus: transactionUpdateItem.transactionId: " + transactionUpdateItem.transactionId);
  admin.firestore().collection(TRANSACTION_TOPIC).doc(transactionUpdateItem.transactionId).set({
    status: transactionUpdateItem.status,
    retry: transactionUpdateItem.retry,
    notes: transactionUpdateItem.notes
  }, {merge: true})
  .then(function() {
      console.log("Success updating transaction with ID:" + transactionUpdateItem.transactionId);
      return null;
  })
  .catch(function(error) {
      console.error("Error writing document with ID: " + transactionUpdateItem.transactionId, error);
  });
  saveTransactionUpdate(transactionUpdateItem);
}

/* Save transaction update in transaction_updates firestore document */
async function saveTransactionUpdate(transactionUpdateItem) {
  const writeResult = await admin.firestore().collection(TRANSACTION_UPDATE_TOPIC).add(transactionUpdateItem)
  console.log(`transactions confirmation from device with ID: ${transactionUpdateItem.transactionId} published.`);
}

/*************************************************************************
* [DEPRECATED] Realtime Database  
**************************************************************************/

exports.onTransactionsCreatedRt = functions.database.ref('/transactions/{transactionId}')
    .onCreate((snapshot, context) => {
      const transactionObject = snapshot.val();
      console.log('transactionsCreatedRt Data: ', JSON.stringify(transactionObject));
      return null;
});

async function saveTransactionToFirestore(transactionObject) {
  const writeResult = await admin.firestore().collection('transactions').add(transactionObject);
  console.log(`transactions ${writeResult.id} published.`);
}