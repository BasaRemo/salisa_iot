module org.kaba {
    requires javafx.controls;
    requires javafx.fxml;

    opens org.kaba to javafx.fxml;
    exports org.kaba;
}