import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    transactions: [],
    loading: false,
    loaded: false
};

const fetchTransStart = (state, action) => {
    return updateObject(state, { loading: true });
}

const fetchTransSuccess = (state, action) => {
    return updateObject(state, {
        transactions: action.transactions,
        loading: false,
        loaded: true
    });
}

const fetchTransFail = (state, action) => {
    return updateObject(state, { loading: false });
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_TRANSACTIONS_START: return fetchTransStart(state, action);
        case actionTypes.FETCH_TRANSACTIONS_SUCCESS: return fetchTransSuccess(state, action);
        case actionTypes.FETCH_TRANSACTIONS_FAIL: return fetchTransFail(state, action);
        default:
            return state;
    }
};
export default reducer;