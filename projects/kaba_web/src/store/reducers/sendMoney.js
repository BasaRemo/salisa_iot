import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility'

const initialState = {
    moneyData: [],
    sending: false
};

const sendMoneyInit = (state, action) => {
    return updateObject(state, { received: false });
}

const sendMoneyStart = (state, action) => {
    return updateObject(state, { sending: true });
}

const sendMoneySuccess = (state, action) => {

    console.log("sendMoneySuccess ", action.moneyDataSent)

    return updateObject(state, {
        sending: true,
        received: true,
        moneyData: action.moneyDataSent
    });
}

const sendMoneyFail = (state, action) => {
    return updateObject(state, { sending: false });
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SEND_MONEY_INIT: return sendMoneyInit(state, action);
        case actionTypes.SEND_MONEY_START: return sendMoneyStart(state, action);
        case actionTypes.SEND_MONEY_SUCCESS: return sendMoneySuccess(state, action);
        case actionTypes.SEND_MONEY_FAIL: return sendMoneyFail(state, action);
        default:
            return state;
    }
};

export default reducer;