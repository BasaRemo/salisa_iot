import * as actionTypes from './actionTypes';


export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        userId: userId
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const logoutSuccess = () => {
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

export const logout = () => {

    localStorage.removeItem('expirationTime');
    localStorage.removeItem('userId');
    return (dispatch, getState, { getFirebase }) => {
        const firebase = getFirebase();
        firebase.auth().signOut().then(function () {
            console.log("Sign-out successful");
            dispatch(logoutSuccess());
        }).catch(function (error) {
            console.log("An error happened.");
        });

    };
};

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000);
    };
};

export const auth = (email, password, isSignup) => {
    return (dispatch, getState, { getFirebase }) => {
        dispatch(authStart());

        const firebase = getFirebase();
        firebase.auth().signInWithEmailAndPassword(
            email, password
        ).then(response => {
            console.log("LOGIN WORKS");
            //const user = firebase.auth().currentUser;
            const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', response.user.uid);
            dispatch(authSuccess(response.user.uid));

            //logout after 1h
            dispatch(checkAuthTimeout(3600));

        }).catch((error) => {
            console.log(error.message);
            dispatch(authFail(error.message));
        })

    };
};


export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    };
};

export const authCheckState = () => {
    return dispatch => {
        const userId = localStorage.getItem('userId');
        if (!userId) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(logout());
            } else {
                //const userId = localStorage.getItem('userId');
                dispatch(authSuccess(userId));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    };

};