import * as actionTypes from './actionTypes';


export const sendMoneyStart = () => {
    return {
        type: actionTypes.SEND_MONEY_START
    };
};

export const sendData = (moneyData, userId) => {

    return (dispatch, getState, { getFirebase, getFirestore }) => {
        dispatch(sendMoneyStart());

        const timestamp = new Date().getTime();
        const docId = userId.concat(timestamp);

        const firestore = getFirestore();
        firestore.collection('transactions').add(
            {
                ...moneyData,
                createAt: new Date(),
                status: "Order placed"
            }).then(() => {
                console.log("response");
                dispatch(sendMoneySuccess(moneyData))
            })
            .catch(error => {
                console.log(error.message);
                dispatch(sendMoneyFail(error.message))
            });
    };
}

export const sendMoneySuccess = (moneyData) => {
    return {
        type: actionTypes.SEND_MONEY_SUCCESS,        
        moneyDataSent: moneyData
    };
};

export const sendMoneyFail = (error) => {
    return {
        type: actionTypes.SEND_MONEY_FAIL,
        error: error
    };
};

export const sendMoneyInit = () => {
    return {
        type: actionTypes.SEND_MONEY_INIT
    };
};