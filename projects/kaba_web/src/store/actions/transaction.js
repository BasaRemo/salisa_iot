import * as actionTypes from './actionTypes';


export const fetchTransSuccess = (transactions) => {
    return {
        type: actionTypes.FETCH_TRANSACTIONS_SUCCESS,
        transactions: transactions
    };
};

export const fetchTransFail = (error) => {
    return {
        type: actionTypes.FETCH_TRANSACTIONS_FAIL,
        error: error
    };
};

export const fetchTransStart = () => {
    return {
        type: actionTypes.FETCH_TRANSACTIONS_START,
    };
};


export const fetchTrans = (userId) => {

    return (dispatch, getState, { getFirebase, getFirestore }) => {

        dispatch(fetchTransStart());
        const firestore = getFirestore();

        // firestore.collection('transactions')
        //     .where('userId', '==', userId)
        //     .orderBy('firstName')
        //     .limit(30)
        //     .get()
        //     .then(querySnapshot => {
        //         const transactionsRecv = querySnapshot.docs
        //             .map(doc => (
        //                 { ...doc.data(), docId: doc.id }));

        //         //console.log(transactionsRecv); // array of transactions objects
        //         dispatch(fetchTransSuccess(transactionsRecv));
        //     })
        //     .catch((error) => {
        //         console.log("userId : ", userId);
        //         console.log("Error fetchTrans : ", error);

        //     })

        // firestore.collection('transactions').where('userId', '==', 'cMHfKT6wSeUGZsRlwND3VhuRSg02')
        // .onSnapshot(querySnapshot => {
        //   querySnapshot.docChanges().forEach(change => {
        //     if (change.type === 'added') {
        //       console.log('New city: ', change.doc.data());
        //     }
        //     if (change.type === 'modified') {
        //       console.log('Modified city: ', change.doc.data());
        //     }
        //     if (change.type === 'removed') {
        //       console.log('Removed city: ', change.doc.data());
        //     }
        //   });
        // });

    }
}