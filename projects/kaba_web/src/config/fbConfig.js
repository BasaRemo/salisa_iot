import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
//import 'firebase/analytics';

//Initialize Firebase

 const firebaseConfig = {
    apiKey: "AIzaSyAG-biOveznQvd0-KOHoLbhPRS6xMokyXQ",
    authDomain: "salisa.firebaseapp.com",
    databaseURL: "https://salisa.firebaseio.com",
    projectId: "salisa",
    storageBucket: "salisa.appspot.com",
    messagingSenderId: "195840489567",
    appId: "1:195840489567:web:1be35d3010909b8320c3ea",
    measurementId: "G-0EDQC4XHBB"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
//firebase.analytics();
firebase.firestore().settings({});

export default firebase;