import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';


import { withStyles } from '@material-ui/core/styles';
import { cardStyles } from '../../../shared/utility';


function Transaction(props) {
    const { classes } = props;
    const bull = <span className={classes.bullet}>•</span>;

    return (
        <Container maxWidth="xs"  >
            <Card  >
                <CardContent >
                    <Typography variant="body2" component="p">
                        Create At: {props.createAt}
                    </Typography>
                    <Typography variant="body2" component="p">
                        Send to: {props.firstName} {props.lastName}
                    </Typography>
                    {/* <Typography variant="body2" component="p">
                        LastName: {props.lastName}
                    </Typography> */}
                    <Typography variant="body2" component="p">
                        Country: {props.country}
                    </Typography>
                    <Typography variant="body2" component="p">
                        Provider: {props.provider}
                    </Typography>
                    <Typography variant="body2" component="p">
                        Amount: {props.amount}
                    </Typography>

                    <Typography variant="body2" component="p">
                        Status: {props.status}
                    </Typography>
                </CardContent>

            </Card>
        </Container>
    )

};

export default withStyles(cardStyles)(Transaction);