import React, { Component } from 'react';
import TransactionItem from './TransactionItem/TransactionItem';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { updateObject } from '../../shared/utility';
import { getFirestore } from 'redux-firestore'

class Transactions extends Component {


    state = {
        transactions: [],
        isFirstQuery: true
    }

    componentDidMount() {
        this.listenForTransactions();
        this.setState({ isFirstQuery: false })
        console.log('isFirstQuery:false ');
    }

    listenForTransactions() {
        const firestore = getFirestore();
        firestore.collection('transactions').where('userId', '==', this.props.userId)
            .orderBy('createAt')
            .onSnapshot(querySnapshot => {
                querySnapshot.docChanges().forEach(change => {
                    if (change.type === 'added') {

                        const time = change.doc.data().createAt.toDate().toGMTString(); // format date
                        const doc = updateObject(change.doc.data(), { 'createAt': time, 'docId': change.doc.id });

                        const newTransactions = this.state.transactions.concat(doc);
                        console.log('New transaction : ', newTransactions);

                        this.setState({ transactions: newTransactions })


                    }
                    if (change.type === 'modified') {
                        console.log('Modified transaction: ', change.doc.data());

                        const myTransactions = [...this.state.transactions];

                        const myTransactionsUpdated = Object.keys(myTransactions)
                            .map(key => {
                                if (myTransactions[key].docId === change.doc.id) {
                                    return updateObject(myTransactions[key], { 'status': change.doc.data().status });
                                }
                                else return myTransactions[key];
                            })

                        this.setState({ transactions: myTransactionsUpdated })
                    }
                    if (change.type === 'removed') {
                        console.log('Removed tranction: ', change.doc.data());
                    }
                });
            });
    }



    render() {

        let transacs = (
            <div >
                <Button variant="contained" color="primary" disableElevation>
                    Disable elevation
                </Button>
            </div>);

        if (this.state.transactions) {

            transacs = this.state.transactions.slice(0).reverse().map(transaction => (
                <TransactionItem
                    key={transaction.docId}
                    firstName={transaction.firstName}
                    lastName={transaction.lastName}
                    phoneNbr={transaction.phoneNbr}
                    provider={transaction.providerSelected}
                    country={transaction.countrySelected}
                    amount={transaction.amount}
                    createAt={transaction.createAt}
                    status={transaction.status}
                />
            ))
        }
        else if (this.props.loading && !this.props.loaded) {
            transacs = (
                <div >
                    <Button variant="contained" color="primary" disableElevation>
                        LOGIN TO ACCESS TO YOUR TRANSACTIONS
                </Button>
                </div>);
        }

        return (
            <div >
                {transacs}
            </div>
        );
    }
};

const mapStateToProps = state => {
    return {
        transactions: state.transaction.transactions,
        sending: state.sendMoney.sending
        //loaded: state.transaction.loaded,
        //userId: state.auth.userId
    }
}
const mapDispatchToDrops = dispatch => {
    return {
        onFetchTransacs: (userId) => dispatch(actions.fetchTrans(userId))
    };
};

export default connect(mapStateToProps, mapDispatchToDrops)(Transactions);