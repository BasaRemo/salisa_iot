import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { myStyles } from '../../shared/utility';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';


const providers = [
    { value: 'vodacom', label: 'Vodacom' },
    { value: 'airtel', label: 'Airtel' }
]

class Provider extends Component {

    render() {
        const { classes } = this.props;

        return (
            <FormControl variant="outlined" className={classes.form}>
                <InputLabel id="provider_id">Provider</InputLabel>
                <Select
                    labelId="provider_id"
                    value={this.props.provider}
                    onChange={this.props.providerHandleChange}
                    label="Provider"

                >
                    {providers.map((provider) => (
                        <MenuItem key={provider.value} value={provider.label} >
                            {provider.label}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        );
    }
}

export default ((withStyles(myStyles, { withTheme: true }))(Provider));