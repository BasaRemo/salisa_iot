import React, { Component } from 'react';
import countryList from 'react-select-country-list'
import { updateObject } from '../../shared/utility'
import Container from '@material-ui/core/Container';
import * as actions from '../../store/actions/index';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { myStyles } from '../../shared/utility';
import Country from './Country';
import Provider from './Provider';
import Name from './Name';

//const countries_ = countryList().getData();



class SendMoney extends Component {

    state = {
        countrySelected: '',
        providerSelected: '',
        firstName: '',
        lastName: '',
        phoneNbr: '',
        amount: ''
    };

    countryHandler = event => {
        const stateUpdated = updateObject(this.state, { countrySelected: event.target.value });
        this.setState(stateUpdated);
    }

    providerHandler = event => {
        const stateUpdated = updateObject(this.state, { providerSelected: event.target.value });
        this.setState(stateUpdated);
    }

    firstNHandler = event => {
        const stateUpdated = updateObject(this.state, { firstName: event.target.value });
        this.setState(stateUpdated);
        //console.log(event.target.value)
    }

    lastNHandler = event => {
        const stateUpdated = updateObject(this.state, { lastName: event.target.value });
        this.setState(stateUpdated);
        //this.setState({ lastName: event.target.value })
        //console.log(event.target.value)
    }

    phoneNbrChangedHandler = (event) => {
        const stateUpdated = updateObject(this.state, { phoneNbr: event.target.value });
        this.setState(stateUpdated);
        //console.log(this.state)
    }

    amountChangedHandler = (event) => {
        const stateUpdated = updateObject(this.state, { amount: event.target.value });
        this.setState(stateUpdated);
        //console.log(this.state)
    }

    sendHandler = (event) => {

        event.preventDefault();
        //const order = updateObject(this.state, { userId: this.props.userId })
        const dataToSend = updateObject(this.state, { userId: this.props.userId })
        this.props.onSendData(dataToSend, this.props.userId);
    }

    render() {

        const { classes } = this.props;

        return (

            <Container maxWidth="xs">

                <div className={classes.paper}>

                    <Typography component="h1" variant="h5">  SEND MONEY </Typography>

                    <form onSubmit={this.sendHandler}>
                        <Country country={this.state.countrySelected} countryHandleChange={this.countryHandler} />

                        <Provider provider={this.state.providerSelected} providerHandleChange={this.providerHandler} />

                        <Name
                            firstName={this.state.firstName}
                            lastName={this.state.lastName}
                            firstNHandleChange={this.firstNHandler}
                            lastNHandleChange={this.lastNHandler}
                        />

                        <TextField
                            variant="outlined"
                            required
                            label="Phone number"
                            fullWidth
                            className={classes.margin_top}
                            onChange={(event) => this.phoneNbrChangedHandler(event)}
                        />

                        <TextField
                            className={classes.margin_top}
                            variant="outlined"
                            required
                            label="Amount"
                            fullWidth
                            onChange={(event) => this.amountChangedHandler(event)}
                        />

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            disabled={!this.props.userId}

                        >
                            SEND
                         </Button>
                    </form>
                </div>

            </Container >


        )
    }
};


const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        loading: state.sendMoney.loading,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSendData: (dataToSend, userId) => dispatch(actions.sendData(dataToSend, userId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(myStyles, { withTheme: true }))(SendMoney));