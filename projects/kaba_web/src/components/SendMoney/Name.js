import React, { Component } from 'react';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import { myStyles } from '../../shared/utility';
import { withStyles } from '@material-ui/core/styles';

class Name extends Component {

    render() {
        const { classes } = this.props;

        return (
            <Box className={classes.name} >
                <Box width="49%" >
                    <TextField
                        variant="outlined"
                        required
                        label="Firstname"
                        onChange={this.props.firstNHandleChange}
                    />
                </Box >
                <Box width="49%" >
                    <TextField
                        variant="outlined"
                        required
                        label="Lastname"
                        onChange={this.props.lastNHandleChange}
                    />
                </Box>
            </Box>
        )
    }
};

export default ((withStyles(myStyles, { withTheme: true }))(Name));
