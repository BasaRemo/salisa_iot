import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { myStyles } from '../../shared/utility';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';

const countries = [
    { value: 'CD', label: 'Congo, République démocratique du' },
    { value: 'CM', label: 'Cameroun' },
    { value: 'SN', label: 'Sénégal' }
]

class Country extends Component {    

    render() {
        const { classes } = this.props;

        return (
            <FormControl variant="outlined" className={classes.form}>
                <InputLabel id="country_id">Country</InputLabel>
                <Select
                    labelId="country_id"
                    value={this.props.country}
                    onChange={this.props.countryHandleChange}
                    label="Country"

                >
                    {countries.map((country) => (
                        <MenuItem key={country.value} value={country.label} >
                            {country.label}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        );
    }

}

export default ((withStyles(myStyles, { withTheme: true }))(Country));