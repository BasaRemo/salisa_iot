import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));



function Mytoolbar(props) {
    const classes = useStyles();

    const backToHomePage = (event) => {
        event.preventDefault();
        props.history.push('/');
    }

    const login = (event) => {
        event.preventDefault();
        props.history.push('/login');
    }

    const logout = (event) => {
        event.preventDefault();
        props.history.push('/logout');
    }


    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        <Link href="#" onClick={backToHomePage} color="inherit" style={{ textDecoration: 'none' }}>
                            KABAPay
                            </Link>
                    </Typography>

                    {props.isAuth ?
                        <Button color="inherit" onClick={logout}>Logout</Button> :
                        <Button color="inherit" onClick={login}>Login</Button>
                    }
                </Toolbar>
            </AppBar>
        </div>
    );

}

export default Mytoolbar