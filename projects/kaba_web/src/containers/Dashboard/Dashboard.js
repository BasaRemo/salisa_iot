import React, { Component } from 'react';
import SendMoney from '../../components/SendMoney/SendMoney';
import Transactions from "../../components/Transactions/Transactions";
import { connect } from 'react-redux';
import { gridStyles } from '../../shared/utility';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


class Dashboard extends Component {

    state = {
        transactions: [],
    }

    render() {

        const { classes } = this.props;
        let dashTransac = (
            <div >
                <Button variant="contained" color="primary" disableElevation>
                    Disable elevation
            </Button>
            </div>
        );

        if (this.props.isAuthenticated) {
            dashTransac = (
                <Paper className={classes.paper}><Transactions userId={this.props.userId} /></Paper>
            )
        }


        return (
            <div className={classes.root} >
                <Grid
                    container
                    spacing={3}
                    direction="row"
                    alignItems="center"
                    justify="center"
                >

                    <Grid item xs={6}>
                        <Paper className={classes.paper}><SendMoney /></Paper>
                    </Grid>
                    <Grid item xs={6}>
                        {dashTransac}
                    </Grid>

                </Grid>

            </div >
        )
    }
};

const mapStateToProps = state => {

    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.userId !== null,
        authRedirectPath: state.auth.authRedirectPath,
        userId: state.auth.userId        
    };
};

export default connect(mapStateToProps, null)((withStyles(gridStyles, { withTheme: true }))(Dashboard));