import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import firebase from 'firebase/app'


import { ReactReduxFirebaseProvider, firebaseReducer, getFirebase } from 'react-redux-firebase'
import { reduxFirestore, createFirestoreInstance, firestoreReducer, getFirestore } from 'redux-firestore'
import firebaseConfig from './config/fbConfig'


import thunk from 'redux-thunk';
import authReducer from '../src/store/reducers/auth';
import sendMoneyReducer from '../src/store/reducers/sendMoney'
import transactionReducer from '../src/store/reducers/transaction'

const rootReducer = combineReducers({
  auth: authReducer,
  sendMoney: sendMoneyReducer,
  transaction: transactionReducer,
  firebase: firebaseReducer,
  firestore: firestoreReducer
});


const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk.withExtraArgument({ getFirebase, getFirestore })),
  reduxFirestore(firebase, firebaseConfig)
));

const rrfProps = {
  firebase,
  config: firebaseConfig,
  dispatch: store.dispatch,
  createFirestoreInstance // <- needed if using firestore
}


const app = (

  <Provider store={store}>
    <ReactReduxFirebaseProvider {...rrfProps}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ReactReduxFirebaseProvider>
  </Provider>


)

ReactDOM.render(app, document.getElementById('root'));

serviceWorker.register();
