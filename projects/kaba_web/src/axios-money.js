
import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://salisa.firebaseio.com/'
})

export default instance
