import React, { Component } from 'react';


import Logout from './containers/Auth/Logout';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import asyncComponent from './hoc/asyncComponent/asyncComponent';
import Dashboard from './containers/Dashboard/Dashboard';
import * as actions from './store/actions/index';
import { connect } from 'react-redux';
import Layout from './hoc/Layout/Layout';
import { ThemeProvider as MuiThemeProvider} from '@material-ui/core/styles';
import MyTheme from './theme/muiTheme';


const asyncLogin = asyncComponent(() => {
  return import('./containers/Auth/Login');
});

class App extends Component {

  componentDidMount() {
    this.props.onTryAutoSignup();
  }

  render() {

    console.log("APP");
    console.log(this.props.isAuthenticated);

    let routes = (
      <Switch>
        <Route path="/login" component={asyncLogin} />
        <Route path="/" exact component={Dashboard} />
        <Redirect to="/" />
      </Switch>
    );

    if (this.props.isAuthenticated) {
      routes = (
        <Switch>
          <Route path="/logout" component={Logout} />
          <Route path="/" exact component={Dashboard} />
          <Redirect to="/" />
        </Switch>
      )
    }

    return (
      <MuiThemeProvider theme={MyTheme}>
        <Layout >
          {routes}
        </Layout>
      </MuiThemeProvider>
    )
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.userId !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));