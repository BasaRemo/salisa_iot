import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {
      light: '#7880ff',
      main: '#2c53ff',
      dark: '#002aca',
      contrastText: '#fff',
    },
  },
});

export default theme;