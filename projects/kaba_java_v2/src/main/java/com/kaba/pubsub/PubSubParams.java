package com.kaba.pubsub;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class PubSubParams {
    public static final String PROJECT_ID = "salisa";
    public static final String SUBSCRIPTION_ID = "cod243_sub";
    public static final String TOPIC_ID = "transactions_updates";

}
