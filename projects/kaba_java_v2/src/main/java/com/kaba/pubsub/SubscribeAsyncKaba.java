/*
 * Copyright 2016 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kaba.pubsub;

// [START pubsub_quickstart_subscriber]
// [START pubsub_subscriber_async_pull]

import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.gson.Gson;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;
import com.kaba.command.AirtelMoneyCmd;
import com.kaba.command.CommandInterface;
import com.kaba.device.Device;
import com.kaba.device.DeviceScan;
import com.kaba.transaction.ManagerTransaction;
import com.kaba.transaction.Transaction;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.concurrent.TimeoutException;

public class SubscribeAsyncKaba implements Runnable {

    public void run() {

        Subscriber subscriber = null;
        try {
            ProjectSubscriptionName subscriptionName =
                    ProjectSubscriptionName.of(PubSubParams.PROJECT_ID, PubSubParams.SUBSCRIPTION_ID);

            // Instantiate an asynchronous message receiver.
            MessageReceiver receiver =
                    (PubsubMessage message, AckReplyConsumer consumer) -> {
                        // Handle incoming message, then ack the received message.

                        String transactionStr = message.getData().toStringUtf8();
                        System.out.println("Id: " + message.getMessageId());

                        Transaction transaction = buildTransaction(transactionStr);
                        System.out.println("Data: " + transaction.toString());

                        //TODO: log transaction
                        consumer.ack(); // send ack
                        ManagerTransaction.getTransactionsQueue().add(transaction);// add transaction to managerTransaction
                    };


            subscriber = Subscriber.newBuilder(subscriptionName, receiver).build();
            // Start the subscriber.
            subscriber.startAsync().awaitRunning();
            System.out.printf("Listening for messages on %s:\n", subscriptionName.toString());
            // Allow the subscriber to run for 30s unless an unrecoverable error occurs.
            subscriber.awaitTerminated();
        } catch (Exception e) {
            // Shut down the subscriber after 30s. Stop receiving messages.
            subscriber.stopAsync();

            e.printStackTrace();
        }
    }

    private Transaction buildTransaction(String transactionJson) {
        Gson gson = new Gson();
        Transaction transaction = gson.fromJson(StringEscapeUtils.unescapeJson(transactionJson), Transaction.class);
        return transaction;
    }
}
// [END pubsub_subscriber_async_pull]
// [END pubsub_quickstart_subscriber]
