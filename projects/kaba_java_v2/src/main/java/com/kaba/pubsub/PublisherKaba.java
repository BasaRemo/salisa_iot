/*
 * Copyright 2016 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kaba.pubsub;

// [START pubsub_quickstart_publisher]

import com.google.api.core.ApiFuture;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.gson.Gson;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.TopicName;
import com.kaba.transaction.Transaction;
import lombok.Data;

import java.io.IOException;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


@Data
public class PublisherKaba implements Runnable {


    private enum PUBLISH_STATE {
        IDLE, WAITING_ANSWER, PROCESSING, DISCONNECTED
    }


    private PUBLISH_STATE publishState = PUBLISH_STATE.IDLE;

    private static Queue<Transaction> messageQueue = new ConcurrentLinkedQueue<Transaction>();
    private List<String> messagePending = new CopyOnWriteArrayList<String>();
    private boolean killThread = false;


    public void run() {

        while (!killThread) {

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            while (messageQueue.isEmpty()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            if ((!messageQueue.isEmpty())) {
                try {

                    Gson gson = new Gson();
                    String responseJson = gson.toJson(messageQueue.poll());

                    publisherKaba(PubSubParams.TOPIC_ID, responseJson);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    public static void publisherKaba(String topicId, String message)
            throws IOException, ExecutionException, InterruptedException {
        TopicName topicName = TopicName.of(PubSubParams.PROJECT_ID, topicId);

        Publisher publisher = null;
        try {
            // Create a publisher instance with default settings bound to the topic
            publisher = Publisher.newBuilder(topicName).build();

            //String message = "Hello World22!";
            ByteString data = ByteString.copyFromUtf8(message);
            PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

            // Once published, returns a server-assigned message id (unique within the topic)
            ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
            String messageId = messageIdFuture.get();

            if (messageId.isEmpty()) {
                messageQueue.poll();
            }
            System.out.println("Published message ID: " + messageId);
        } finally {
            if (publisher != null) {
                // When finished with the publisher, shutdown to free up resources.
                publisher.shutdown();
                publisher.awaitTermination(1, TimeUnit.MINUTES);
            }
        }
    }

    public static Queue<Transaction> getMessageQueue() {
        return messageQueue;
    }

    public static void setMessageQueue(Queue<Transaction> messageQueue) {
        PublisherKaba.messageQueue = messageQueue;
    }

//    public static Queue<String> getMessageQueue() {
//        return messageQueue;
//    }
//
//    public boolean isKillThread() {
//        return killThread;
//    }
//
//    public void setKillThread(boolean killThread) {
//        this.killThread = killThread;
//    }
//
//    public List<String> getMessagePending() {
//        return messagePending;
//    }
//
//    public void setMessagePending(List<String> messagePending) {
//        this.messagePending = messagePending;
//    }
}

