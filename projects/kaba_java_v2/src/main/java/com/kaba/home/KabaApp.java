package com.kaba.home;

import com.kaba.controller.MainWindowController;
import com.kaba.device.DeviceScan;
import com.kaba.exec.Exec;
import com.kaba.pubsub.PublisherKaba;
import com.kaba.pubsub.SubscribeAsyncKaba;
import com.kaba.transaction.ManagerTransaction;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class KabaApp extends Application {

    private MainWindowController controller;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        Scene scene = new Scene(new MainWindowController());
        controller = (MainWindowController) scene.getRoot();
        primaryStage.setScene(scene);

        primaryStage.show();

    }

    @Override
    public void init() {

        Exec.getExec().execute(new PublisherKaba());
        Exec.getExec().execute(new SubscribeAsyncKaba());
        Exec.getExec().execute(new DeviceScan());
        Exec.getExec().execute(new ManagerTransaction());

        //ManagerTransaction.getInstance();
    }
}
