package com.kaba.util;

import java.time.LocalDateTime;

public class Util {

    public static synchronized long getRunTime() {
        return System.nanoTime() / 1_000_000L;
    }

}
