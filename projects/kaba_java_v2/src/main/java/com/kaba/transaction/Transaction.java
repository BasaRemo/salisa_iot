package com.kaba.transaction;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Transaction {

    enum TransactionStatus {
        BEGIN,                      // Transaction is paid for but not processed on firestore/pub/sub yet
        PROCESSING,                 // When transaction send to firestore
        PROCESSING_ERROR,           // When we fail to deliver message from pubsub to java
        NETWORK_SENT,               // when we receive ussd OK response
        NETWORK_PROCESSING_ERROR,   // when ussd returns ERROR
        NETWORK_COMPLETED,          // When we received the sms confirmation
    }

    private String id;
    private String transactionId;
    private String firstName;
    private String lastName;
    private String country;
    private String status;
    private String provider;
    private String amount;
    private String phone_number;
    private int retry = 0;
    private String networkResponse = "";
    private TransactionStatus transactionStatus;


}
