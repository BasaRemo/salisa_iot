package com.kaba.transaction;


import com.kaba.command.AirtelMoneyCmd;
import com.kaba.command.CommandInterface;
import com.kaba.device.Device;
import com.kaba.device.DeviceScan;
import com.kaba.device.ManagerDevice;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


public class ManagerTransaction implements Runnable {

    private static Queue<Transaction> transactionsQueue = new ConcurrentLinkedQueue<Transaction>();

    public ManagerTransaction() {

    }


    private void sendToDevice(CommandInterface cmd) {

        if (cmd != null) {
            String network = cmd.getTransaction().getProvider();

            Device device = ManagerDevice.getDeviceByNetwork(network);

            if (device != null) {
                device.getDeviceCom().getMessageCmdQueue().add(cmd);
                //TODO: transaction sent to device
            } else {
                //TODO : Log
            }
        }

    }


    @Override
    public void run() {

        while (!Thread.currentThread().isInterrupted()) {

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            while (transactionsQueue.size() == 0) {

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }

            if (transactionsQueue.size() > 0) {
                Transaction transaction = transactionsQueue.poll();
                CommandInterface cmd = null;

                switch (transaction.getProvider().toLowerCase()) {
                    case "airtel":
                        cmd = new AirtelMoneyCmd(transaction, "1151");

                        break;
                    case "vodacom":

                        break;

                    case "orange":

                        break;
                    default:
                        break;
                }

                sendToDevice(null);

            }
        }

    }

    public static Queue<Transaction> getTransactionsQueue() {
        return transactionsQueue;
    }

    public static void setTransactionsQueue(Queue<Transaction> transactionsQueue) {
        ManagerTransaction.transactionsQueue = transactionsQueue;
    }
}
