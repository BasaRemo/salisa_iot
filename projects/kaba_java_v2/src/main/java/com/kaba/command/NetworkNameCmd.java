package com.kaba.command;

import com.kaba.device.Device;
import com.kaba.device.DeviceCom;

import java.util.ArrayList;
import java.util.List;

/**
 * Description : Network Name USSDMODULE
 */

public class NetworkNameCmd implements CommandInterface {

    private String networkName;

    public NetworkNameCmd() {
        networkName = "";
    }

    @Override
    public boolean processCommand(List<String> response, Object threadCalling) {

        try {
            if (threadCalling.getClass() != DeviceCom.class)
                return false;

            Device device = ((DeviceCom) threadCalling).getDevice();
            String response_str = response.get(0);
            String m_networkName = response_str.substring(response_str.indexOf('"') + 1, response_str.lastIndexOf('"'));

            System.out.println("setNetworkName " + m_networkName);
            device.getNetworkName().set(m_networkName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public int getCommandLength() {
        return 0;
    }

    @Override
    public int getCommandNumber() {
        return 0;
    }

    @Override
    public List<String> listAtCommand() {
        List<String> listAtCmd = new ArrayList<>();
        String networkName = "AT+COPS?";
        listAtCmd.add(networkName);
        return listAtCmd;
    }
}
