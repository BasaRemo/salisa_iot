package com.kaba.command;

import com.kaba.device.Device;
import com.kaba.device.DeviceCom;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

public class CustomCmd implements CommandInterface {

    private String atCmd;

    private final StringProperty response_at = new SimpleStringProperty();;

    public CustomCmd(String cmd) {
        this.atCmd = cmd;
    }

    @Override
    public boolean processCommand(List<String> response, Object threadCalling) {

        try {
            if (threadCalling.getClass() != DeviceCom.class)
                return false;

            Device device = ((DeviceCom) threadCalling).getDevice();
            String response_str = response.get(0);

            device.setCustomCmdRes(response_str);
            this.setResponse_at(response_str);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public int getCommandLength() {
        return 0;
    }

    @Override
    public int getCommandNumber() {
        return 0;
    }

    @Override
    public List<String> listAtCommand() {
        List<String> listAtCmd = new ArrayList<>();
        System.out.println("CMD "+atCmd);
        listAtCmd.add(atCmd);
        return listAtCmd;
    }

    public final String getResponse_at() {
        return response_at.get();
    }

    public final StringProperty response_atProperty() {
        return response_at;
    }

    public final void setResponse_at(String response_at) {
        this.response_at.set(response_at);
    }
}
