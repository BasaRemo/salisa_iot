package com.kaba.command;

import com.kaba.pubsub.PublisherKaba;
import com.kaba.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;

public class AirtelMoneyCmd implements CommandInterface {


    private String phone_number;
    private String amount;
    private String code;
    private Transaction transaction;

    public AirtelMoneyCmd(String phone_number, String amount, String code) {
        this.phone_number = phone_number;
        this.amount = amount;
        this.code = code;
    }

    public AirtelMoneyCmd(Transaction transaction, String code) {

        this.transaction = transaction;
        this.phone_number = transaction.getPhone_number();
        this.amount = amount;
        this.code = code;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean processCommand(List<String> response, Object threadCalling) {

        transaction.setNetworkResponse(response.get(response.size() - 1));

        PublisherKaba.getMessageQueue().add(transaction);

        return true;
    }

    @Override
    public int getCommandLength() {
        return 0;
    }

    @Override
    public int getCommandNumber() {
        return 0;
    }

    @Override
    public List<String> listAtCommand() {
        List<String> listAtCmd = new ArrayList<>();


        String command0 = "AT+CUSD=1,\"*501#\",15";
        String command1 = "AT+CUSD=1,\"2\",15";
        String command2 = "AT+CUSD=1,\"1\",15";
        String command3 = "AT+CUSD=1,\"1\",15";
        String command4 = "AT+CUSD=1,\"" + phone_number + "\",15"; //"AT+CUSD=1,\"0971504436\",15";
        String command5 = "AT+CUSD=1,\"" + amount + "\",15"; //"AT+CUSD=1,\"2500\",15";
        String command6 = "AT+CUSD=1,\"" + code + "\",15"; //"AT+CUSD=1,\"1151\",15";
        String command7 = "AT+CUSD=1,\"2\",15";
        String setGsmModemToTextMode = "AT+CMGF=1";
        String listUnreadSMS = "AT+CMGL=\"REC UNREAD\"";


        listAtCmd.add(command0);
        listAtCmd.add(command1);
        listAtCmd.add(command2);
        listAtCmd.add(command3);
        listAtCmd.add(command4);
        listAtCmd.add(command5);
        listAtCmd.add(command6);
        listAtCmd.add(command7);
        listAtCmd.add(setGsmModemToTextMode);
        listAtCmd.add(listUnreadSMS);
        return listAtCmd;
    }

    @Override
    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
