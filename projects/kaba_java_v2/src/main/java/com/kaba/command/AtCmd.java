package com.kaba.command;

import java.util.ArrayList;
import java.util.List;

/**
 * Description : Check if  the device is a USSDMODULE
 */

public class AtCmd implements CommandInterface {

    public AtCmd() {
    }

    @Override
    public int getCommandLength() {
        return 0;
    }

    @Override
    public int getCommandNumber() {
        return 0;
    }

    @Override
    public List<String> listAtCommand() {
        List<String> listAtCmd = new ArrayList<>();
        listAtCmd.add("AT");
        return listAtCmd;
    }
}
