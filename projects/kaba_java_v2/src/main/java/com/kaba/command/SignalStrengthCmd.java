package com.kaba.command;

import com.kaba.device.Device;
import com.kaba.device.DeviceCom;

import java.util.ArrayList;
import java.util.List;

/**
 * Description : getSignalStrength USSDMODULE
 */
public class SignalStrengthCmd implements CommandInterface {


    private double signalStrength;

    public SignalStrengthCmd() {
        signalStrength = 0;
    }

    @Override
    public boolean processCommand(List<String> response, Object threadCalling) {

        try {
            if (threadCalling.getClass() != DeviceCom.class)
                return false;

            Device device = ((DeviceCom) threadCalling).getDevice();


            String response_str = response.get(0);
            String d_signalStrength = response_str.replace("+CSQ:", "");
            d_signalStrength = d_signalStrength.replace("OK", "").trim();
            d_signalStrength = d_signalStrength.replace(",", ".");
            System.out.println("signalStrength " + d_signalStrength);
            //signalStrength = Double.parseDouble(d_signalStrength);

            device.getSignalStrength().set(d_signalStrength);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public double getSignalStrength() {

        return signalStrength;
    }

    public void setSignalStrength(double signalStrength) {
        this.signalStrength = signalStrength;
    }

    @Override
    public int getCommandLength() {
        return 0;
    }

    @Override
    public int getCommandNumber() {
        return 0;
    }

    @Override
    public List<String> listAtCommand() {
        List<String> listAtCmd = new ArrayList<>();
        String getSignalStrength = "AT+CSQ";
        listAtCmd.add(getSignalStrength);
        return listAtCmd;
    }
}
