package com.kaba.command;

import java.util.ArrayList;
import java.util.List;

/**
 * Description : Setup USSDMODULE
 */
public class SetupCmd implements CommandInterface {

    public SetupCmd() {
    }

    @Override
    public int getCommandLength() {
        return 0;
    }

    @Override
    public int getCommandNumber() {
        return 1;
    }

    @Override
    public List<String> listAtCommand() {
        List<String> listAtCmd = new ArrayList<>();
        String testAtCommand = "AT";
        String setGsmModemToPDUMode = "AT+CMGF=0"; // O: PDU Mode,  1: Text Mode
        String setCharacterSetToGSM = "AT+CSCS=\"GSM\"";
        String getSignalStrength = "AT+CSQ";
        String registrationStatusOptions = "AT+CREG=?";
        String registrationStatus = "AT+CREG?";
        String enableUssdCodePresentation = "AT+CUSD=1";
        String networkName = "AT+COPS?";
        String smsStorage = "AT+CPMS=?";
        String storeSmsInSim = "AT+CPMS=\"SM\",\"SM\",\"SM\"";


        listAtCmd.add(testAtCommand);
        listAtCmd.add(setGsmModemToPDUMode);
        listAtCmd.add(setCharacterSetToGSM);
        listAtCmd.add(getSignalStrength);
        listAtCmd.add(registrationStatusOptions);
        listAtCmd.add(registrationStatus);
        listAtCmd.add(enableUssdCodePresentation);
        listAtCmd.add(networkName);
        listAtCmd.add(smsStorage);
        listAtCmd.add(storeSmsInSim);

        return listAtCmd;
    }
}
