package com.kaba.command;

import com.kaba.device.Device;
import com.kaba.device.DeviceCom;

import java.util.ArrayList;
import java.util.List;

public class PhoneNumberCmd implements CommandInterface {
    @Override
    public int getCommandLength() {
        return 0;
    }

    @Override
    public int getCommandNumber() {
        return 0;
    }

    @Override
    public List<String> listAtCommand() {
        List<String> listAtCmd = new ArrayList<>();
        String getSignalStrength = "AT+CNUM";
        listAtCmd.add(getSignalStrength);
        return listAtCmd;
    }

    @Override
    public boolean processCommand(List<String> response, Object threadCalling) {

        try {
            if (threadCalling.getClass() != DeviceCom.class)
                return false;

            Device device = ((DeviceCom) threadCalling).getDevice();
            String response_str = response.get(0);
            response_str = response_str.split(",")[1];
            String num = response_str.substring(1, response_str.length() - 1);
            device.getPhoneNumber().set(num);

            System.out.println(num);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

}
