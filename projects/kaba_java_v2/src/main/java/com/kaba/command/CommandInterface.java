package com.kaba.command;


import com.kaba.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Command use for the communication between KABAJAVA and Device
 *
 * <br>
 * <br>
 * IMPORTANT ALL COMMAND NEED TO HAVE A CONSTRUCTOR WITHOUT PARAMETERS
 */
public interface CommandInterface {

    int getCommandLength();

    int getCommandNumber();

    List<String> listAtCommand();

    default boolean processCommand(List<String> response, Object threadCalling) {
        return true;
    }

    default Transaction getTransaction() {
        return null;
    }

}
