package com.kaba.exec;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

public class Exec {

    private static final int POOL_THREAD = 20;

    private static ThreadPoolExecutor exec;

    public static synchronized ThreadPoolExecutor getExec() {
        if (exec == null) {
            exec = (ThreadPoolExecutor) Executors.newFixedThreadPool(POOL_THREAD);
        }
        return exec;
    }

}
