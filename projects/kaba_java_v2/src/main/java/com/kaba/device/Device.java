package com.kaba.device;

import com.fazecast.jSerialComm.SerialPort;
import javafx.beans.property.ObjectProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Device {

    public enum DEVICE_STATUS {
        UNAVAILABLE("Unavailable"),
        INITIALIZING("Initializing"),
        FREE("Free"),
        BUSY("Busy");

        private String statusName;

        private DEVICE_STATUS(String status) {
            this.statusName = status;
        }

        @Override
        public String toString() {
            return statusName;
        }
    }

    public enum PROVIDER {
        UNAVAILABLE("Unavailable"),
        VODACOM("Vodacom"),
        AIRTEL("Airtel"),
        ORANGE("Orange"),
        FIDO("Fido");

        private String provName;

        private PROVIDER(String prov) {
            this.provName = prov;
        }

        @Override
        public String toString() {
            return provName;
        }
    }

    private String id;
    private DEVICE_STATUS status = DEVICE_STATUS.UNAVAILABLE;
    private DeviceCom deviceCom;
    private PROVIDER provider = PROVIDER.UNAVAILABLE;
    private ObjectProperty<String> comName;
    private ObjectProperty<String> networkName;
    private SerialPort port;
    private ObjectProperty<String> signalStrength;
    private boolean isReady;
    private String customCmdRes;
    private ObjectProperty<String> phoneNumber;

    public String getStatusText() {
        switch (status) {
            case UNAVAILABLE:
                return DEVICE_STATUS.UNAVAILABLE.toString();
            case INITIALIZING:
                return DEVICE_STATUS.INITIALIZING.toString();
            case FREE:
                return DEVICE_STATUS.FREE.toString();
            case BUSY:
                return DEVICE_STATUS.BUSY.toString();
            default:
                return "N/A";
        }

    }


    public String getProviderText() {
        switch (getProvider()) {
            case UNAVAILABLE:
                return PROVIDER.UNAVAILABLE.toString();
            case VODACOM:
                return PROVIDER.VODACOM.toString();
            case ORANGE:
                return PROVIDER.ORANGE.toString();
            case AIRTEL:
                return PROVIDER.AIRTEL.toString();
            case FIDO:
                return PROVIDER.FIDO.toString();
            default:
                return "N/A";
        }

    }
}
