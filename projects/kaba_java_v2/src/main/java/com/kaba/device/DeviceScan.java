package com.kaba.device;

import com.fazecast.jSerialComm.SerialPort;
import com.kaba.command.*;
import com.kaba.util.Util;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class DeviceScan implements Runnable {

    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(30);
    //private static List<SerialPort> portListOpened = new ArrayList<SerialPort>();
    private static List<SerialPort> portListAll = new ArrayList<SerialPort>();
    private static long startTime = 0;
    //private static ObservableList<Device> myObserv = FXCollections.observableArrayList()
    //private static List<Device> listDevices = Collections.synchronizedList(new ArrayList<>());


    @Override
    public void run() {

        startTime = Util.getRunTime();

        while (!Thread.currentThread().isInterrupted()) {

            try {
                Thread.sleep(4000); // scan each 4 secondes

                while ((Util.getRunTime() - startTime) > 60000) { // stop scanning after 1 minute
                    //System.out.println("STOP SCANNING ... ");
                    //TODO: log stop scanning
                    Thread.sleep(5000);
                }

                for (SerialPort port : SerialPort.getCommPorts()) {

                    System.out.println("--DEVICESCAN --PORT " + port.getSystemPortName());

                    if (!portListAll.contains(port)) {

                        port.setComPortParameters(115200, 8, 1, SerialPort.NO_PARITY);

                        if (port.openPort()) {

                            Device device = Device.builder().id(Long.toString(Util.getRunTime())).port(port)
                                    .status(Device.DEVICE_STATUS.UNAVAILABLE).signalStrength(new SimpleObjectProperty<String>(""))
                                    .networkName(new SimpleObjectProperty<String>(""))
                                    .phoneNumber(new SimpleObjectProperty<String>(""))
                                    .comName(new SimpleObjectProperty<String>(port.getSystemPortName())).build();

                            DeviceCom deviceCom = new DeviceCom(device);

                            ManagerDevice.getDeviceComList().add(deviceCom);

                            device.setDeviceCom(deviceCom);

                            //String testAtCommand = "AT";

                            ManagerDevice.getDevicesList().add(device);
                            //observableListDevices.add(device);
                            //portListOpened.add(port);
                            executor.execute(deviceCom);

                            //TODO: log run DeviceCOm
                            deviceCom.getMessageCmdQueue().add(new AtCmd());
                            //deviceCom.getMessageCmdQueue().add(new SetupCmd());
                            deviceCom.getMessageCmdQueue().add(new SignalStrengthCmd());
                            deviceCom.getMessageCmdQueue().add(new PhoneNumberCmd());
                            deviceCom.getMessageCmdQueue().add(new NetworkNameCmd());

                            //System.out.println("PORT ADDED TO LIST " + port.getSystemPortName());

                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public static void reScan() {
        startTime = Util.getRunTime();
    }

}
