package com.kaba.device;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ManagerDevice {


    private static ObservableList<Device> devicesList = FXCollections.synchronizedObservableList(FXCollections.observableArrayList(new Callback<Device, Observable[]>() {
        @Override
        public Observable[] call(Device device) {
            return new Observable[]{device.getPhoneNumber(), device.getSignalStrength(), device.getNetworkName()};
        }
    }));

    private static ObservableList<DeviceCom> deviceComList = FXCollections.synchronizedObservableList(FXCollections.observableArrayList(
            new Callback<DeviceCom, Observable[]>() {
                @Override
                public Observable[] call(DeviceCom deviceCom) {
                    return new Observable[]{deviceCom.logOnUiProperty()};
                }
            }
    ));


    public static Device getDeviceByNetwork(String network) {

        for (Device device : devicesList) {
            if (device.getNetworkName().getValue().toLowerCase().contains(network.toLowerCase())) {
                return device;
            }
        }
        return null;
    }


    public static ObservableList<Device> getDevicesList() {
        return devicesList;
    }

    public static ObservableList<DeviceCom> getDeviceComList() {
        return deviceComList;
    }


}
