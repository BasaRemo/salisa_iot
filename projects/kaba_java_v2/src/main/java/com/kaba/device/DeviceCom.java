package com.kaba.device;

import com.fazecast.jSerialComm.SerialPort;
import com.kaba.command.CommandInterface;
import com.kaba.util.Util;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class DeviceCom implements Runnable {


    private enum DEVICE_STATE {
        ONLINE, DISCONNECTED
    }

    private enum COM_STATE {
        LOAD, IDLE, SEND, READ, PROCESS
    }


    private Device device;
    private SerialPort port;
    private boolean isListenerInit;
    private Queue<CommandInterface> messageCmdQueue = new ConcurrentLinkedQueue<CommandInterface>();
    private Queue<String> messageAtQueue = new ConcurrentLinkedQueue<String>();
    private List<String> responseList;


    private boolean killThread = false;
    private boolean isUssdModule = false;
    private boolean isConnected = false;
    private long timeConnection = 0;
    private long startTime = 0;
    private CommandInterface actualCommand;
    private ObjectProperty<String> logOnUi = new SimpleObjectProperty<String>("");

    private String atCmdSent = "";
    private DEVICE_STATE deviceState = DEVICE_STATE.DISCONNECTED;
    private COM_STATE comState = COM_STATE.LOAD;

    public DeviceCom(Device device) {
        startTime = Util.getRunTime();
        this.device = device;
        this.port = device.getPort();

    }


    @Override
    public void run() {

        while (!killThread) {
            //System.out.println("RUNNING  " + port.getSystemPortName()+ "STATE "+comState);
            try {
                Thread.sleep(50);
                switch (comState) {

                    case LOAD:
                        List<String> atCmdList;

                        if (!messageCmdQueue.isEmpty()) {
                            actualCommand = messageCmdQueue.poll();
                            atCmdList = actualCommand.listAtCommand();
                            for (String atCmd : atCmdList) {
                                messageAtQueue.add(atCmd);
                            }
                            responseList = new ArrayList<String>();
                            comState = COM_STATE.SEND;
                            System.out.println("LOAD  " + port.getSystemPortName());
                        } else {
                            while (messageCmdQueue.isEmpty()) {
                                Thread.sleep(4000);
                            }

                        }

                        break;

                    case SEND:
                        if (!messageAtQueue.isEmpty()) {
                            atCmdSent = messageAtQueue.poll();
                            logOnUi.set("SEND " + atCmdSent);
                            sendUSSD(atCmdSent);
                            System.out.println("DEVICECOM--" + device.getComName() + "--SEND:" + atCmdSent);
                            comState = COM_STATE.READ;
                        } else
                            comState = COM_STATE.PROCESS;
                        break;

                    case READ:
                        startTime = Util.getRunTime();
                        while (port.bytesAvailable() == 0) {
                            Thread.sleep(20);
                            if ((Util.getRunTime() - startTime) > 6000) { // 46000ms == 4 sec
                                killThread = true;
                                break;
                            }
                        }
                        System.out.println("DEVICECOM--" + device.getComName() + "--READ");
                        if (!killThread && port.bytesAvailable() > 0) {
                            byte[] readBuffer = new byte[port.bytesAvailable()];
                            port.readBytes(readBuffer, readBuffer.length);

                            String message = new String(readBuffer);
                            System.out.println("DEVICECOM--" + device.getComName() + "CMD: " + atCmdSent + "--READ--RESPONSE :" + message);

                            logOnUi.set("READ " + message);
                            responseList.add(message);
                            comState = COM_STATE.SEND;
                        }

                        if (port.bytesAvailable() < 0) {
                            System.out.println("DEVICECOM--" + device.getComName() + "CMD: " + atCmdSent + "--READ--BYTEAVAILABLE :" + port.bytesAvailable());
                        }
                        break;

                    case PROCESS:
                        actualCommand.processCommand(responseList, this);
                        comState = COM_STATE.LOAD;
                        break;
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public void setKillThread(boolean killThread) {
        this.killThread = killThread;
    }


    public Queue<CommandInterface> getMessageCmdQueue() {
        return messageCmdQueue;
    }

    private void sendUSSD(String msg) {
        try {
            port.getOutputStream().write(msg.getBytes());
            port.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getAtCmdSent() {
        return atCmdSent;
    }

    public void setAtCmdSent(String atCmdSent) {
        this.atCmdSent = atCmdSent;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public String getLogOnUi() {
        return logOnUi.get();
    }

    public ObjectProperty<String> logOnUiProperty() {
        return logOnUi;
    }

    public void setLogOnUi(String log) {
        this.logOnUi.set(log);
    }
}
