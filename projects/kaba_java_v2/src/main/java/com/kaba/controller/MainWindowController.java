package com.kaba.controller;

import com.kaba.command.CommandInterface;
import com.kaba.command.CustomCmd;
import com.kaba.device.Device;
import com.kaba.device.DeviceCom;
import com.kaba.device.DeviceScan;
import com.kaba.device.ManagerDevice;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.Duration;


import java.io.IOException;

public class MainWindowController extends VBox {
    private static final String FXML_DOCUMENT = "/MainWindow.fxml";
    private static final String SCENE_STYLESHEET = "/Scene.css";
    //private ObservableList<Device> deviceList = FXCollections.observableArrayList();
    private ObservableList<String> networkNameList = FXCollections.observableArrayList();
    private static final double NO_SECONDS_BETWEEN_REFRESHES = 4;
    private Device deviceSelected = null;
    private ObservableList<Device> deviceList;

    @FXML
    private TableView<Device> deviceTable;


    @FXML
    private TableColumn<Device, String> com;
    @FXML
    private TableColumn<Device, String> deviceNetwork;
    @FXML
    private TableColumn<Device, String> deviceSignal;
    @FXML
    private TableColumn<Device, String> deviceStatus;
    @FXML
    private TableColumn<Device, String> devicePhoneNbr;
    @FXML
    private ComboBox<String> deviceCbox;
    @FXML
    private TextField atCommandText;
    @FXML
    private Button atCommandBtn;
    @FXML
    private TextArea atCmdConsole;
    @FXML
    private TextArea devicesLog;

    public MainWindowController() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(FXML_DOCUMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    public void initialize() {

        ManagerDevice.getDeviceComList().addListener(new ListChangeListener<DeviceCom>() {
            @Override
            public void onChanged(Change<? extends DeviceCom> c) {
                while (c.next()) {
                    if (c.wasPermutated()) {
                        for (int i = c.getFrom(); i < c.getTo(); ++i) {
                            System.out.println("DVCOM Permuted: " + i + " " + ManagerDevice.getDeviceComList().get(i).logOnUiProperty().get());
                        }
                    } else if (c.wasUpdated()) {
                        for (int i = c.getFrom(); i < c.getTo(); ++i) {
                            String tempStr = ManagerDevice.getDeviceComList().get(i).logOnUiProperty().get();
                            Platform.runLater(() -> devicesLog.appendText(tempStr + "\n"));
                            System.out.println("DVCOM Updated: " + i + " " + ManagerDevice.getDeviceComList().get(i).logOnUiProperty().get());
                        }
                    } else {
                        for (DeviceCom removedItem : c.getRemoved()) {
                            System.out.println("Removed: " + removedItem);
                        }
                        for (DeviceCom addedItem : c.getAddedSubList()) {
                            Platform.runLater(() -> devicesLog.appendText(addedItem + "\n"));
                            System.out.println("DVCOM Added: " + addedItem);
                        }
                    }
                }
            }
        });

        ManagerDevice.getDevicesList().addListener(new ListChangeListener<Device>() {

            @Override
            public void onChanged(Change<? extends Device> c) {
                while (c.next()) {

                    if (c.wasUpdated()) {
                        for (int i = c.getFrom(); i < c.getTo(); ++i) {
                            //System.out.println("Updated: " + i + " " + "fruit.get(i)");
                        }
                    } else {

                        for (Device item : c.getAddedSubList()) {
                            deviceTable.setItems(ManagerDevice.getDevicesList());

                        }
                    }
                    deviceTable.refresh();
                }
            }
        });


        //deviceTable.setItems(deviceList);
        deviceTable.setEditable(false);

        // Device Com
        com.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Device, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Device, String> p) {
                return new SimpleStringProperty(p.getValue().getComName().getValue());
            }
        });
        com.setCellFactory(TextFieldTableCell.forTableColumn());
        com.setStyle("-fx-alignment: CENTER");

        // Device name
        deviceNetwork.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Device, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Device, String> p) {
                return new SimpleStringProperty(p.getValue().getNetworkName().getValue());
            }
        });
        deviceNetwork.setCellFactory(TextFieldTableCell.forTableColumn());
        deviceNetwork.setStyle("-fx-alignment: CENTER");


        // Device phone number
        devicePhoneNbr.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Device, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Device, String> p) {
                String pnbr = p.getValue().getPhoneNumber().getValue();

                if (!pnbr.isEmpty() && !networkNameList.contains(pnbr))
                    Platform.runLater(() -> networkNameList.add(pnbr));
                return new SimpleStringProperty(p.getValue().getPhoneNumber().getValue());
            }
        });
        devicePhoneNbr.setCellFactory(TextFieldTableCell.forTableColumn());
        devicePhoneNbr.setStyle("-fx-alignment: CENTER");


        // Signal
        deviceSignal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Device, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Device, String> p) {
                return new SimpleStringProperty(p.getValue().getSignalStrength().getValue());
            }
        });
        deviceSignal.setCellFactory(TextFieldTableCell.forTableColumn());
        deviceSignal.setStyle("-fx-alignment: CENTER");


        deviceStatus.setCellValueFactory(new PropertyValueFactory<>("statusText"));
        deviceStatus.setCellFactory(column -> {
            return new TableCell<Device, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    setGraphic(null);
                    if (item != null && !empty) {
                        Device device = getTableView().getItems().get(getIndex());

                        if (!isEmpty()) {

                            switch (device.getStatus()) {
                                case UNAVAILABLE:
                                    setStyle("-fx-background-color: #ff000073;-fx-alignment: CENTER");
                                    break;
                                case INITIALIZING:
                                    setStyle("-fx-background-color: #00800073;-fx-alignment: CENTER");
                                    break;
                                case FREE:
                                    setStyle("-fx-background-color: #00800073;-fx-alignment: CENTER");
                                    break;

                                case BUSY:
                                    setStyle("-fx-background-color: #00800073;-fx-alignment: CENTER");
                                    break;
                                default:
                                    setStyle("-fx-background-color: #ff000073;-fx-alignment: CENTER");
                                    break;
                            }
                        }
                    }
                }
            };
        });
        deviceStatus.setEditable(false);

        deviceCbox.setItems(networkNameList);

        deviceCbox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String v1, String v2) {

                //System.out.println(t1);

                for (Device device : ManagerDevice.getDevicesList()) {
                    if (v2.contains(device.getPhoneNumber().getValue()) && !device.getPhoneNumber().getValue().isEmpty()) {

                        //System.out.println("ZZZZ " + v2);
                        //System.out.println("device.getPhoneNumber() " + device.getPhoneNumber());
                        deviceSelected = device;
                        break;
                    }
                }
            }
        });

        //atCmdConsole.setText("CONSOLE");

        atCommandBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //System.out.println("AAA "+atCommandText.getText());
                CommandInterface customCmd = new CustomCmd(atCommandText.getText());
                deviceSelected.getDeviceCom().getMessageCmdQueue().add(customCmd);

                ((CustomCmd) customCmd).response_atProperty().addListener(new ChangeListener<String>() {

                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        //System.out.println(MessageFormat.format("The name changed from \"{0}\" to \"{1}\"", oldValue, newValue));
                        //atCmdConsole.appendText(newValue+"\n");
                        Platform.runLater(() -> atCmdConsole.appendText(newValue + "\n"));
                    }

                });
            }

        });

    }


}
