## SalisaPay

- Send Money to any mobile money provider in Africa!
- This repo has multiple branches master which has the cloud server logic and kaba-windows which has the local server in africa that reads/writes SIM cards USSD commands to our servers and the Telecoms servers

### AT Commands example: 
https://m2msupport.net/m2msupport/ussd-how-to-send-ussd-short-codes-with-at-command/

#### Send USSD 

- List of supported response
	- AT+CUSD=?

- Status of result code presentation. 0: disabled, 1: enabled
	- AT+CUSD?

- Disable USSD result code presentation
	- AT+CUSD=0


- Enable USSD result code presentation
	- AT+CUSD=1


- Cancel session
	- AT+CUSD=2

- Set the character set to GSM
	- AT+CSCS="GSM"

- USSD Code to see Orange Money Menu

	- AT+CUSD=1,"*101*3*4*1#",15


- If you send unsupport code, you will see cancel reponse session +CUSD :2
	- AT+CUSD=1,"unsupported code",15
	
	### Project Architecure 

![Scheme](https://bitbucket.org/BasaRemo/salisa_iot/raw/master/images/kaba-architecture.jpg)